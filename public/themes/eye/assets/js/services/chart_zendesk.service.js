(function () {
    'use strict';

    reportApp.factory('chartzendeskService', chartzendeskService);
    function chartzendeskService($http, $q) {

        var service = {};
        var http_path = 'http://report.senangpay.my';

        service.getdata = function (data) {
            var m = $q.defer();

            $http({
                url: http_path + '/report/zendesk',
                cache: false,
                method: "GET"
            }).then(function (response) {
                m.resolve(response.data);
            },function (result) {
                m.reject(result);
            });
            return m.promise;
        };

        return service;

    }
})();
