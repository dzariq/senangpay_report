(function () {
    'use strict';

    reportApp.factory('chartsignupService', chartsignupService);
    function chartsignupService($http, $q) {

        var service = {};
   //     var http_path = 'http://localhost/senangpay_report/senangpay_report/public';
      var http_path = 'http://report.senangpay.my';

        service.getdata = function (data) {
            var m = $q.defer();

            $http({
                url: http_path + '/report/signup',
                cache: false,
                method: "GET"
            }).then(function (response) {
                m.resolve(response.data);
            },function (result) {
                m.reject(result);
            });
            return m.promise;
        };

        service.getdataprogress = function (data) {
            var m = $q.defer();

            $http({
                url: http_path + '/report/getmonthlybyyear',
                cache: false,
                params: data,
                method: "GET"
            }).then(function (response) {
                m.resolve(response.data);
            },function (result) {
                m.reject(result);
            });
            return m.promise;
        };

        return service;

    }
})();
