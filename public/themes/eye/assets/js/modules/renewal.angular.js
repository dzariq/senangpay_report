reportApp.controller('renewalCtrl', function ($scope, $interval, chartrenewalService) {

//run every 1min
    $interval(getData, 60000);

    getData()

    function getData() {
        $scope.daily = {}
        $scope.monthly = {}
        $scope.yearly = {}

        chartrenewalService.getdata().then(function (data) {
           console.log(data)
            $scope.daily.total = data.daily.total
            $scope.daily.total_amount = data.daily.total_amount
            $scope.daily.total_status = data.daily.total_status
            $scope.daily.total_status_amount = data.daily.total_amount_status
            $scope.daily.total_status_percent = data.daily.total_status_percent
            $scope.daily.total_status_amount_percent = data.daily.total_status_amount_percent

            $scope.monthly.total = data.monthly.total
            $scope.monthly.total_amount = data.monthly.total_amount
            $scope.monthly.total_status = data.monthly.total_status
            $scope.monthly.total_amount_status = data.monthly.total_amount_status
            $scope.monthly.total_status_percent = data.monthly.total_status_percent
            $scope.monthly.total_status_amount_percent = data.monthly.total_status_amount_percent

            $scope.yearly.total = data.yearly.total
            $scope.yearly.total_amount = data.yearly.total_amount
            $scope.yearly.total_status = data.yearly.total_status
            $scope.yearly.total_amount_status = data.yearly.total_amount_status
            $scope.yearly.total_status_percent = data.yearly.total_status_percent
            $scope.yearly.total_status_amount_percent = data.yearly.total_status_amount_percent
        })

    }

});