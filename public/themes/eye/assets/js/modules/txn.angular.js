reportApp.controller('txnCtrl', function ($scope, $interval, charttxnService) {

//run every 1min
    $interval(getData, 60000);

    getData()

    function getData() {
        $scope.daily = {}
        $scope.monthly = {}
        $scope.yearly = {}

        charttxnService.getdata().then(function (data) {
            $scope.apm = data.apm
            $scope.lastfivemin = data.lastfivemin
            console.log($scope.lastfivemin[0].merchant_name)
            $scope.lastfivemintotal = data.lastfivemintotal
            $scope.daily.total = data.daily.total
            $scope.daily.total_amount = data.daily.total_amount
            $scope.daily.total_amount_yesterday = data.daily.total_amount_yesterday
            $scope.daily.total_status = data.daily.total_status
            $scope.daily.total_amount_status = data.daily.total_amount_status
            $scope.daily.total_status_percent = data.daily.total_status_percent
            $scope.daily.total_amount_status_percent = data.daily.total_amount_status_percent

            $scope.monthly.total = data.monthly.total
            $scope.monthly.total_lastmonth = data.monthly.total_lastmonth
            $scope.monthly.total_amount = data.monthly.total_amount
            $scope.monthly.total_status = data.monthly.total_status
            $scope.monthly.total_amount_status = data.monthly.total_amount_status
            $scope.monthly.total_status_percent = data.monthly.total_status_percent
            $scope.monthly.total_amount_status_percent = data.monthly.total_amount_status_percent

            $scope.yearly.total = data.yearly.total
            $scope.yearly.total_amount = data.yearly.total_amount
            $scope.yearly.total_status = data.yearly.total_status
            $scope.yearly.total_amount_status = data.yearly.total_amount_status
            $scope.yearly.total_status_percent = data.yearly.total_status_percent
            $scope.yearly.total_amount_status_percent = data.yearly.total_amount_status_percent

            $scope.colors1 = ["#338ECC", "#1D336C", "#14CD7D"];
            $scope.label1 = ["Basic", "Advanced", "Special"];
            $scope.data1 = [data.monthly.total_amount_basic, data.monthly.total_amount_advanced, data.monthly.total_amount_special];
console.log($scope.data1);
            $scope.colors2 = ["#1293DE", "#EF991A","FF0000","FFFF00"];
            $scope.label2 = ["FPX", "Credit Card","Boost","TNG"];
            $scope.data2 = [data.monthly.total_amount_fpx, data.monthly.total_amount_cc, data.monthly.total_amount_boost, data.monthly.total_amount_tng];

            $scope.colors3 = ["#DEEDF8", "#EF6A83"];
            $scope.label3 = ["Native", "Affiliate"];
            $scope.data3 = [data.monthly.total_amount_nonaffiliate, data.monthly.total_amount_affiliate];

            $scope.options1 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };

            $scope.options2 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };

            $scope.options3 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };
            $scope.options4 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };
            $scope.options5 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };
            $scope.options6 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };

//            $scope.options1 = {
//                legend: {display: true},
//            };
//            $scope.options2 = {
//                legend: {display: true},
//            };
//            $scope.options3 = {
//                legend: {display: true},
//            };
        })

    }

});