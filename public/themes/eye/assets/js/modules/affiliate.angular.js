reportApp.controller('affiliateCtrl', function ($scope, $interval, chartaffiliateService) {

//run every 1min
    $interval(getData, 300000);

    getData()

    function getData() {
        $scope.daily = {}
        $scope.monthly = {}
        $scope.yearly = {}
        $scope.alltime = {}
        $scope.average = {}

        chartaffiliateService.getdata().then(function (data) {
            console.log(data)
            $scope.daily.total = data.daily.total
            $scope.daily.total_amount = data.daily.total_amount
            $scope.daily.total_status = data.daily.total_status
            $scope.daily.total_status_amount = data.daily.total_amount_status
            $scope.daily.total_status_percent = data.daily.total_status_percent
            $scope.daily.total_status_amount_percent = data.daily.total_status_amount_percent

            $scope.monthly.total = data.monthly.total
            $scope.monthly.total_amount = data.monthly.total_amount
            $scope.monthly.total_status = data.monthly.total_status
            $scope.monthly.total_amount_status = data.monthly.total_amount_status
            $scope.monthly.total_status_percent = data.monthly.total_status_percent
            $scope.monthly.total_status_amount_percent = data.monthly.total_status_amount_percent

            $scope.yearly.total = data.yearly.total
            $scope.yearly.total_amount = data.yearly.total_amount
            $scope.yearly.total_status = data.yearly.total_status
            $scope.yearly.total_amount_status = data.yearly.total_amount_status
            $scope.yearly.total_status_percent = data.yearly.total_status_percent
            $scope.yearly.total_status_amount_percent = data.yearly.total_status_amount_percent

            $scope.alltime.merchant_affiliate = data.alltime.merchant_affiliate;
            $scope.alltime.registered_by_affiliate = data.alltime.registered_by_affiliate;
            $scope.alltime.signup_value = data.alltime.signup_value;
            $scope.alltime.transaction_value = data.alltime.transaction_value;

            $scope.average.merchant_affiliate = data.average.merchant_affiliate;
            $scope.average.merchant_affiliate_percentage = data.average.merchant_affiliate_percentage;
            $scope.average.merchant_affiliate_percentage_status = data.average.merchant_affiliate_percentage_status;
            $scope.average.signup_value = data.average.signup_value;
            $scope.average.transaction_value = data.average.transaction_value;
            $scope.average.signup_value_percentage_status = data.average.signup_value_percentage_status;
            $scope.average.transaction_value_percentage_status = data.average.transaction_value_percentage_status;
            $scope.average.signup_value_percentage = data.average.signup_value_percentage;
            $scope.average.transaction_value_percentage = data.average.transaction_value_percentage;

            console.log(data.average.merchant_affiliate)

           $scope.colors = ["#338ECC", "#1D336C", "#14CD7D"];
            $scope.label = ["Basic", "Advanced", "Special"];
            $scope.data = [data.alltime.txn_value_basic,data.alltime.txn_value_advanced,data.alltime.txn_value_special];
//            $scope.options = {
//                legend: {display: true},
//            };
            console.log($scope.data)

            $scope.options = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };
        })



    }

});