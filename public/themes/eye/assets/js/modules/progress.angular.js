reportApp.controller('progressCtrl', function ($scope, $interval, chartsignupService) {

    $scope.category = 'signup';
    $scope.package = '';
    $scope.affiliate = '';
    $scope.from = '';
    $scope.to = '';
    $scope.type = 'count';
    $scope.payment_type = '';
    $scope.maxvalue = 0
    $scope.grand_total = 0

    var dt = new Date();

    $scope.getData = function () {
        $scope.load = true;
        if ($scope.from == '') {
            var from_year = dt.getFullYear()
            var from_month = 1
        } else {
            var from_year = $scope.from.getFullYear()
            var from_month = $scope.from.getMonth() + 1
        }
        if ($scope.to == '') {
            var to_year = dt.getFullYear()
            var to_month = 12
        } else {
            var to_year = $scope.to.getFullYear()
            var to_month = $scope.to.getMonth() + 1
        }

        var datapush = {
            'type': $scope.type,
            'category': $scope.category,
            'affiliate': $scope.affiliate,
            'package': $scope.package,
            'payment_type': $scope.payment_type,
            'to_year': to_year,
            'to_month': to_month,
            'from_year': from_year,
            'from_month': from_month,
        }
        console.log(datapush)
        chartsignupService.getdataprogress(datapush).then(function (data) {
            $scope.load = false;

            $scope.labels = []
            $scope.data = [];

            if ($scope.category == 'mdr_cc' || $scope.category == 'mdr_fpx') {
                $scope.mdr = [];
                $scope.total_amount = [];
                $scope.net_income = [];
                $scope.merchant_mdr = [];

                $.each(data, function (i, val) {
                    $scope.labels.push(i)
                    $scope.mdr.push(val['mdr'])
                    $scope.merchant_mdr.push(val['merchant_mdr'])
                    $scope.net_income.push(val['net_income'])
                    $scope.total_amount.push(val['total_amount'])
                });

                $scope.data = [$scope.merchant_mdr, $scope.mdr, $scope.net_income]
                $scope.series = ['MERCHANT MDR', 'MDR', 'NET INCOME']
            } else if ($scope.category == 'income') {
                $scope.total_amount = [];

                $.each(data, function (i, val) {
                    $scope.labels.push(i)
                    $scope.total_amount.push(val['total_amount'])
                });

                $scope.data = [$scope.total_amount]
                $scope.series = ['TOTAL INCOME']

                $scope.grand_total = 0
                $.each(data, function (i, val) {
                    console.log(val)
                    $scope.grand_total += parseFloat(val.total_amount)
                });
            } else if ($scope.category == 'processing_fee') {
                $scope.total_amount = [];

                $.each(data, function (i, val) {
                    $scope.labels.push(i)
                    $scope.total_amount.push(val['total_amount'])
                });

                $scope.data = [$scope.total_amount]
                $scope.series = ['PROCESSING FEE']

                $scope.grand_total = 0
                $.each(data, function (i, val) {
                    console.log(val)
                    $scope.grand_total += parseFloat(val.total_amount)
                });
            } else {
                $.each(data, function (i, val) {
                    $scope.labels.push(i)
                    $scope.data.push(val)
                    if (val > $scope.maxvalue) {
                        $scope.maxvalue = val
                    }
                });
                console.log($scope.maxvalue)

                $scope.grand_total = 0
                $.each(data, function (i, val) {
                    console.log(val)
                    $scope.grand_total += parseFloat(val)
                });
            }


            console.log($scope.grand_total)

        })



        console.log($scope.maxvalue)
        console.log($scope.data)

        $scope.options = {
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            // stepValue: stepvalue,
                            //  max: $scope.maxvalue
                        }
                    }
                ]
            }
        };


    }

    $scope.getData()


});