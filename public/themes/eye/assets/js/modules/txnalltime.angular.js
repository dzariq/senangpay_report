reportApp.controller('txnalltimeCtrl', function ($scope, $interval, charttxnalltimeService) {

//run every 1min
    $interval(getData, 60000);

    getData()

    function getData() {
        $scope.daily = {}
        $scope.monthly = {}
        $scope.yearly = {}

        charttxnalltimeService.getdata().then(function (data) {
            
            $scope.paid = data.paid
            $scope.total = data.total
            $scope.unique_count = data.unique_count
            $scope.unique_all_status = data.unique_all_status
            $scope.avpt = data.avpt
            console.log(data);
            $scope.colors1 = ["#338ECC", "#1D336C", "#14CD7D"];
            $scope.label1 = ["Basic", "Advanced", "Special"];
            $scope.data1 = [data.basic, data.advanced, data.special];

            $scope.colors2 = ["#1293DE", "#EF991A","FF0000","FFFF00"];
            $scope.label2 = ["FPX", "Credit Card","Boost","TNG"];
            $scope.data2 = [data.fpx, data.cc, data.boost, data.tng];

            $scope.colors3 = ["#DEEDF8", "#EF6A83"];
            $scope.label3 = ["Native", "Affiliate"];
            $scope.data3 = [data.nonaffiliate, data.affiliate];

            $scope.options1 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };

            $scope.options2 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };

            $scope.options3 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };
        })

    }

});