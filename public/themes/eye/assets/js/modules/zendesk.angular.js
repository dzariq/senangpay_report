reportApp.controller('zendeskCtrl', function ($scope, $interval, chartzendeskService) {

//run every 1min
    $interval(getData, 120000);

    getData()

    function getData() {

        chartzendeskService.getdata().then(function (data) {
            console.log(data)
            
            $scope.today_new = data.today.new
            $scope.today_pending = data.today.pending
            $scope.today_solved = data.today.solved
            
            $scope.thismonth_new = data.thismonth.new
            $scope.thismonth_pending = data.thismonth.pending
            $scope.thismonth_solved = data.thismonth.solved
            
            $scope.new_average_daily = data.new_average_daily.data
            $scope.alltime_closed = data.alltime_closed.data
            
        })

    }

});