reportApp.controller('serverCtrl', function ($scope, $interval, chartserverService) {

//run every 1min
    $interval(getData, 300000);

    getData()

    function getData() {

        chartserverService.getdata().then(function (data) {
            console.log(data)
            $scope.chart_server = data;
            $scope.data_memory02 = []
            $scope.labels_memory01 = []
            $scope.labels_memory02 = []
            $scope.data_cpu01 = []
            $scope.data_cpu02 = []
            $scope.labels_cpu01 = []
            $scope.labels_cpu02 = []
            $scope.series_memory01 = ['MEMORY'];
            $scope.series_cpu01 = ['CPU'];
            $scope.series_cpu02 = ['CPU'];

            $scope.data_memory01 = []
            $.each(data[0].memory, function (i, val) {
                $scope.data_memory01.push(val)
                $scope.labels_memory01.push(i)
            });
console.log($scope.data_memory01)
            var z = 1;
            $.each(data[0].cpu, function (i, val) {
                $scope.data_cpu01.push(val)
                $scope.labels_cpu01.push(z)
                z++;
            });

            var z = 1;
            $.each(data[1].memory, function (i, val) {
                $scope.data_memory02.push(val)
                $scope.labels_memory02.push(z)
                z++
            });
            var z = 1;
            $.each(data[1].cpu, function (i, val) {
                $scope.data_cpu02.push(val)
                $scope.labels_cpu02.push(z)
                z++;
            });

            $scope.series_memory02 = ['MEMORY'];
            console.log($scope.data_memory02)

            $scope.options_memory02 = {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                steps: 10,
                                // stepValue: stepvalue,
                                max: 100
                            }
                        }
                    ]
                }
            };

        })

    }

});