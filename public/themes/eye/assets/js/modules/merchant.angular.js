reportApp.controller('merchantCtrl', function ($scope, $interval, chartmerchantService) {

//run every 1min
    $interval(getData, 60000);

    getData()

    function getData() {

        chartmerchantService.getdata().then(function (data) {

            $scope.colors1 = ["#338ECC", "#1D336C", "#14CD7D"];
            $scope.label1 = ["Basic", "Advanced", "Special"];
            $scope.data1 = [data.basic, data.advanced, data.special];
            
            $scope.all = data.total;
            $scope.active = data.active;
            $scope.inactive = data.active;
            $scope.arpu = data.arpu;

            $scope.options1 = {
                cutoutPercentage: 70,
                elements: {
                    arc: {
                        borderWidth: 0 // border line
                    }
                }
            };
        })

    }

});