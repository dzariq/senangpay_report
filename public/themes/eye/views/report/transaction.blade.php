<!-- Main Content -->
<style>

    .lmask {
        position: absolute;
        height: 100%;
        width: 100%; 
        background-color: #000;
        bottom: 0;
        left: 0;
        right: 0;
        top: 0;
        z-index: 9999;;
        opacity: 0.4;
        &.fixed {
            position: fixed;
        }
        &:before {
            content: '';
            background-color: rgba(0,0,0,0);
            border: 5px solid rgba(0,183,229,0.9);
            opacity: .9;
            border-right: 5px solid rgba(0,0,0,0);
            border-left: 5px solid rgba(0,0,0,0);
            border-radius: 50px;
            box-shadow: 0 0 35px #2187e7;
            width: 50px;
            height: 50px;
            -moz-animation: spinPulse 1s infinite ease-in-out;
            -webkit-animation: spinPulse 1s infinite linear;

            margin: -25px 0 0 -25px;
            position: absolute;
            top: 50%;
            left: 50%;
        }
        &:after {
            content: '';
            background-color: rgba(0,0,0,0);
            border: 5px solid rgba(0,183,229,0.9);
            opacity: .9;
            border-left: 5px solid rgba(0,0,0,0);
            border-right: 5px solid rgba(0,0,0,0);
            border-radius: 50px;
            box-shadow: 0 0 15px #2187e7;
            width: 30px;
            height: 30px;
            -moz-animation: spinoffPulse 1s infinite linear;
            -webkit-animation: spinoffPulse 1s infinite linear;

            margin: -15px 0 0 -15px;
            position: absolute;
            top: 50%;
            left: 50%;
        }
    }

    @-moz-keyframes spinPulse {
        0% {
            -moz-transform:rotate(160deg);
            opacity: 0;
            box-shadow: 0 0 1px #2187e7;
        }
        50% {
            -moz-transform: rotate(145deg);
            opacity: 1;
        }
        100% {
            -moz-transform: rotate(-320deg);
            opacity: 0;
        }
    }
    @-moz-keyframes spinoffPulse {
        0% {
            -moz-transform: rotate(0deg);
        }
        100% {
            -moz-transform: rotate(360deg);
        }
    }
    @-webkit-keyframes spinPulse {
        0% {
            -webkit-transform: rotate(160deg);
            opacity: 0;
            box-shadow: 0 0 1px #2187e7;
        }
        50% {
            -webkit-transform: rotate(145deg);
            opacity: 1;
        }
        100% {
            -webkit-transform: rotate(-320deg);
            opacity: 0;
        }
    }
    @-webkit-keyframes spinoffPulse {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

</style>
<section ng-app="report" ng-controller="progressCtrl" class="content">
    <div ng-if="load" class='lmask'></div>
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Progress Chart
                    <small>All Time Progress View</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{ route('_home') }}"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <!--li class="breadcrumb-item"><a href="javascript:void(0);">Sample Pages</a></li-->
                    <li class="breadcrumb-item active">Progress Chart</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Setting</strong> </h2>                        
                        <ul class="header-dropdown">
                            <li><i class="zmdi zmdi-cached"></i>test</li>
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="javascript:void(0);">Clear Setting</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix m-t-30">
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Category</b> </p>
                                <select  class="form-control show-tick" ng-model="category" >
                                    <option value="">Select Catgeory</option>
                                    <option selected='selected' value="signup">Sign Up</option>
                                    <option  value="renewal">Renewal</option>
                                    <option  value="transaction">Paid Transaction</option>
                                    <option  value="transaction_failed">Failed Transaction</option>
                                    <option  value="refund">Refund</option>
                                    <option  value="affiliate">Affiliate</option>
                                    <option  value="mdr_cc">MDR CREDIT CARD</option>
                                    <option  value="mdr_fpx">MDR FPX</option>
                                    <option  value="processing_fee">Processing Fees</option>
                                    <option  value="income">Total Income</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Type</b> </p>
                                <select ng-disabled='category == "affiliate" || category == "mdr_cc" || category == "mdr_fpx" || category == "processing_fee" || category == "income" '  class="form-control show-tick" ng-model="type"  >
                                    <option value="">Select Type</option>
                                    <option  value="value">Value</option>
                                    <option selected='selected' value="count">Counts</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Packages</b> </p>
                                <select ng-disabled='category == "affiliate" || category == "mdr_cc" || category == "mdr_fpx" || category == "processing_fee" || category == "income" '  class="form-control show-tick" ng-model="package"  >
                                    <option selected='selected' value="">All Packages</option>
                                    <option value='basic'>Basic package only</option>
                                    <option value='advanced'>Advanced package only</option>
                                    <option value='special'>Special package only</option>
                                    <option value='monthly'>Monthly package only</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Affiliate</b> </p>
                                <select ng-disabled='category == "affiliate" || category == "mdr_cc" || category == "mdr_fpx" || category == "processing_fee" || category == "income" '  class="form-control show-tick" ng-model="affiliate"  >
                                    <option selected='selected' value="">Affiliates & Non-affiliates</option>
                                    <option value='affiliate'>Affiliates only</option>
                                    <option value='non_affiliate'>Non-affiliates only</option>
                                </select>
                            </div>
                            <div style='margin-top:20px' class="col-lg-3 col-md-6">
                                <p> <b>Payment Type</b> </p>
                                <select ng-disabled='category == "affiliate" || category == "mdr_cc" || category == "mdr_fpx" || category == "processing_fee" || category == "income" '  class="form-control show-tick" ng-model="payment_type"  >
                                    <option selected="selected" value=''>All payment type</option>
                                    <option value='Credit Card'>Debit/Credit card only</option>
                                    <option value='FPX'>FPX only</option>
                                </select>
                            </div>
                            <!--                            <div class="col-lg-3 col-md-6">
                                                            <p> <b>Merchant Status</b> </p>
                                                            <select class="form-control show-tick" ng-model="merchant_status"  >
                                                                <option value=''>All status</option>
                                                                <option value='renewal'>Renewal only</option>
                                                                <option value='terminated'>Terminated only</option>
                                                                <option value='active'>Active only</option>
                                                            </select>
                                                        </div>-->
                            <div  style='margin-top:20px' class="col-lg-3 col-md-6">
                                <p> <b>From :</b> </p>
                                <input  type='month' ng-model='from' class='form-control' />
                            </div>
                            <div  style='margin-top:20px' class="col-lg-3 col-md-6">
                                <p> <b>To :</b> </p>
                                <input  type='month' ng-model='to' class='form-control' />
                            </div>
                       
                            <div  style='margin-top:20px' class="col-lg-3 col-md-6">
                                <button ng-click="getData()" class='btn btn-primary' >Search</button>
                            </div>
                            <!--                            <div class="col-lg-3 col-md-6">
                                                            <p> <b>Display average line</b> </p>
                                                            <select class="form-control show-tick" data-show-subtext="true">
                                                                <option>None</option>
                                                                <option data-subtext="(RM)">Revenue per user</option>
                                                                <option data-subtext="(RM)">Value per transaction</option>
                                                                <option data-subtext="(#)" disabled>Frequency per minute</option>
                                                            </select>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Graph</strong> </h2>                        
                        <h2 ng-if="type == 'count'" class="float-right">
                            TOTAL: <% grand_total | number:2  %>
                        </h2>
                        <h2 ng-if="type == 'value'" class="float-right">
                            TOTAL: RM <% grand_total | number:2  %>
                        </h2>
                    </div>
                    <div class="body" >
                        <div class="row clearfix m-t-30">
                            <div class="col-lg-11 col-md-11">
                                <canvas id="bar"  class="chart chart-bar"
                                        chart-data="data" chart-options="options" chart-labels="labels" chart-series="series">
                                </canvas>

                            </div>
                            <div class="col-lg-1 col-md-1">
<!--                                <p style="margin-bottom: -5px;">Total</p>                            
                                <h3 class="counter m-b-0 col-white">1,332,002</h3>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>