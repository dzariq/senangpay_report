<section ng-app="report" ng-controller="dashboardCtrl"  class="content">
    <div class="container-fluid" style="margin-top: -20px;">
        <div class="row clearfix">

            <!-- SECTION 1 : PERFORMANCE ======================================= -->
            <div class="col-lg-3 col-md-3 col-sm-12 fix-col">
                <div  class="card card-fix-margin bg-dark">
                    <div class="header">
                        <h2><strong>Sign Up</strong> Monthly</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">This Month</a></li>
                                    <li><a href="javascript:void(0);">Previous Month</a></li>
                                    <li><a href="javascript:void(0);">Next Month</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div ng-controller="signupCtrl" class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p style="margin-bottom: -5px;">Value (RM)</p>                            
                                        <h3 class="counter m-b-0 col-white"><% monthly.total_amount %> 
                                            <span style="font-size: 14px;" ng-if='monthly.total_amount_status == "up"'><i  class="zmdi zmdi-caret-up text-success" ></i> <% monthly.total_amount_status_percent %>%</span>
                                            <span style="font-size: 14px;" ng-if='monthly.total_amount_status == "down"'><i  class="zmdi zmdi-caret-down text-danger" ></i> <% monthly.total_amount_status_percent %>%</span>
                                        </h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">&nbsp;</div>
                                    <div class="col-sm-4" style="text-align: right;">
                                        <canvas id="pie" class="chart chart-doughnut" style="width: 180px; height: 90px; float: right; margin-top: -100px;"
                                                chart-data="data" chart-labels="labels" chart-colors="colors" chart-options="options" width="180px" height="90px">
                                        </canvas> 
                                    </div>
                                </div>
                            </div>
                            <div ng-controller="signupCtrl" class="col-sm-2 col-2 m-b-10">
                                <span class="text-muted">Today</span>
                                <h5 class="m-b-0"><% daily.total %></h5>
                                <span ng-if='daily.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% daily.total_status_percent %>%</span>
                                <span ng-if='daily.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% daily.total_status_percent %>%</span>
                            </div>
                            <div ng-controller="signupCtrl" class="col-sm-3 col-3 m-b-10">
                                <span class="text-muted">This Month</span>
                                <h5 class="m-b-0"><% monthly.total %></h5>
                                <span ng-if='monthly.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% monthly.total_status_percent %>%</span>
                                <span ng-if='monthly.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% monthly.total_status_percent %>%</span>
                            </div>
                            <div ng-controller="affiliateCtrl" class="col-sm-2 col-2 m-b-10">
                                <span class="text-muted">Affiliate</span>
                                <h5 class="m-b-0"><% monthly.total %></h5>
                                <span ng-if='monthly.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% monthly.total_status_percent %>%</span>
                                <span ng-if='monthly.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% monthly.total_status_percent %>%</span> 
                            </div>
                            <div ng-controller="renewalCtrl" class="col-sm-2 col-3 m-b-10">
                                <span class="text-muted">Renewal</span>
                                <h5 class="m-b-0"><% monthly.total %></h5>
                                <span ng-if='monthly.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% monthly.total_status_percent %>%</span>
                                <span ng-if='monthly.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% monthly.total_status_percent %>%</span>
                            </div>
                            <!--                            <div  class="col-sm-3 col-3 m-b-10">
                                                            <span class="text-muted">Terminated</span>
                                                            <h5 class="m-b-0"><% monthly.total %></h5>
                                                            <span ng-if='monthly.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% monthly.total_status_percent %>%</span>
                                                            <span ng-if='monthly.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% monthly.total_status_percent %>%</span>
                                                        </div>-->
                        </div> 

                    </div>
                </div>

                <div class="card card-fix-margin bg-dark">
                    <div class="header">
                        <h2><strong>Transaction </strong>Monthly</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">This Month</a></li>
                                    <li><a href="javascript:void(0);">Previous Month</a></li>
                                    <li><a href="javascript:void(0);">Next Month</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div ng-controller="txnCtrl" class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <small class="text-muted">Value Today RM</small>                               
                                        <h3 class="counter m-b-0 col-white"><% daily.total_amount %></h3>
<!--                                        <span ng-if='daily.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% daily.total_status_percent %>%</span>
                                        <span ng-if='daily.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% daily.total_status_percent %>%</span>-->
                                        <span style='color:white'><% daily.total_amount_yesterday %></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <small class="text-muted">Frequency</small>
                                        <h3 class="counter m-b-0 col-white"><% monthly.total %></h3>
<!--                                        <span ng-if='monthly.total_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% monthly.total_status_percent %>%</span>
                                        <span ng-if='monthly.total_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% monthly.total_status_percent %>%</span>-->
                                        <span style='color:white'><% monthly.total_lastmonth %></span>

                                    </div>


                                    <div class="col-sm-6 m-t-10">
                                        <small class="text-muted">Value This Month RM</small>
                                        <h3 class="counter m-b-0 col-white"><% monthly.total_amount %></h3>
                                        <span ng-if='monthly.total_amount_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% monthly.total_amount_status_percent %>%</span>
                                        <span ng-if='monthly.total_amount_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% monthly.total_amount_status_percent %>%</span>
                                    </div>
                                    <div class="col-sm-6 m-t-10">
                                        <small class="text-muted">Avg per min</small>
                                        <h3 class="counter m-b-0 col-white"><% apm %></h3>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-sm-4" >
                                        <span style="margin-bottom:10px">Packages %</span>
                                        <canvas style="margin-top:14px" id="pie1" class="chart chart-doughnut"
                                                chart-data="data1" chart-labels="label1" chart-colors="colors1" chart-options="options1">
                                        </canvas> 
                                    </div>
                                    <div class="col-sm-4" >
                                        <span style="margin-bottom:10px">Payment Type %</span>
                                        <canvas style="margin-top:14px" id="pie2" class="chart chart-doughnut"
                                                chart-data="data2" chart-labels="label2" chart-colors="colors2" chart-options="options2">
                                        </canvas> 
                                    </div>
                                    <div class="col-sm-4" >
                                        <span style="margin-bottom:10px">Affiliates %</span>
                                        <canvas style="margin-top:14px" id="pie3" class="chart chart-doughnut"
                                                chart-data="data3" chart-labels="label3" chart-colors="colors3" chart-options="options3">
                                        </canvas> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-12 m-t-10 p-15 text-center">
                                <div ng-if="lastfivemintotal > 0" class="icon l-green label-doc" style="width: 100%; border-radius: 3px;"><% lastfivemintotal %>  transaction for the last 5 minutes</div>
                                <div ng-if="lastfivemintotal == 0" class="icon l-blush label-doc p-t-5 p-b-5" style="width: 100%; border-radius: 3px;"><% lastfivemintotal %>  transaction for the last 5 minutes</div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover m-b-0">
                                <tbody>
                                    <tr>
                                        <td style="width:40%">Merchant</td>
                                        <td>Value (RM)</td>
                                        <td>Type</td>
                                        <td>Customer</td>
                                        <td>Time</td>
                                    </tr>
                                    <tr ng-repeat='item in lastfivemin'>
                                        <td><% item.merchant_name %></td>
                                        <td><span><% item.price_total %></span></td>
                                        <td ng-if="item.payment_type == 'Credit Card'">CC</td>
                                        <td ng-if="item.payment_type == 'FPX'">FPX</td>
                                        <td ng-if="item.payment_type == 'Boost'">Boost</td>
                                        <td ng-if="item.payment_type == 'Touch N Go'">TnG</td>
                                        <td><% item.customer_name %></td>
                                        <td><% item.created_at %></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div  class="card card-fix-margin bg-dark">
                    <div class="header">
                        <h2><strong>Affiliate</strong> All-Time</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div ng-controller="affiliateCtrl" class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <small class="text-muted">Member</small>                            
                                        <h3 class="counter m-b-0 col-white"><% alltime.merchant_affiliate %>
                                        </h3>
                                    </div>
                                    <div class="col-sm-4">
                                        <small class="text-muted">Produce</small>           
                                        <h3 class="counter m-b-0 col-white"><% alltime.registered_by_affiliate %> 
                                        </h3>

                                    </div>
                                    <div class="col-sm-4">
                                        <small class="text-muted">Packages %</small>   
                                        <canvas style="margin-top:14px" id="pie3" class="chart chart-doughnut"
                                                chart-data="data" chart-labels="label" chart-colors="colors" chart-options="options">
                                        </canvas> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <small class="text-muted">Signup RM</small>                       
                                        <h3 class="counter m-b-0 col-white"><% alltime.signup_value | number:2 %> 
                                        </h3>
                                    </div>
                                    <div class="col-sm-6">
                                        <small class="text-muted">Transactions RM</small>                          
                                        <h3 class="counter m-b-0 col-white"><% alltime.transaction_value | number:2 %> 
                                        </h3>
                                    </div>
                                </div>

                            </div>

                        </div> 

                    </div>
                </div>
                <div  class="card card-fix-margin bg-dark">
                    <div class="header">
                        <h2><strong>Affiliate</strong> Average Monthly</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div ng-controller="affiliateCtrl" class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <small class="text-muted">New Signup</small>                 
                                        <h3 class="counter m-b-0 col-white"><% average.merchant_affiliate %>
                                        </h3>
                                        <span ng-if='average.merchant_affiliate_percentage_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% average.merchant_affiliate_percentage | number:1 %>%</span>
                                        <span ng-if='average.merchant_affiliate_percentage_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% average.merchant_affiliate_percentage | number:1 %>%</span>
                                    </div>
                                    <div class="col-sm-4">
                                        <small class="text-muted">Signup RM</small>                          
                                        <h3 class="counter m-b-0 col-white"><% average.signup_value | number:2 %> 
                                        </h3>
                                        <span ng-if='average.signup_value_percentage_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% average.signup_value_percentage | number:1 %>%</span>
                                        <span ng-if='average.signup_value_percentage_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% average.signup_value_percentage | number:1 %>%</span>
                                    </div>
                                    <div class="col-sm-4">
                                        <small class="text-muted">Transactions RM</small>                     
                                        <h3 class="counter m-b-0 col-white"><% average.transaction_value | number:2 %> 
                                        </h3>
                                        <span ng-if='average.transaction_value_percentage_status == "up"'><i class="zmdi zmdi-caret-up text-success"></i> <% average.transaction_value_percentage | number:1 %>%</span>
                                        <span ng-if='average.transaction_value_percentage_status == "down"'><i class="zmdi zmdi-caret-down text-danger"></i> <% average.transaction_value_percentage | number:1 %>%</span>
                                    </div>
                                </div>

                            </div>

                        </div> 

                    </div>
                </div>
                <style>
                    table td {
                        white-space: nowrap;
                        overflow: hidden;
                        text-overflow: ellipsis;
                        max-width: 40px;
                    }
                </style>
                <div ng-controller="txnalltimeCtrl" class="card card-fix-margin bg-dark">
                    <div class="header">
                        <h2><strong>Transaction</strong> All-time</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">This Year</a></li>
                                    <li><a href="javascript:void(0);">Previous Year</a></li>
                                    <li><a href="javascript:void(0);">Next Year</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-4" >
                                <span style="margin-bottom:10px">Packages %</span>
                                <canvas style="margin-top:14px" id="pie1" class="chart chart-doughnut"
                                        chart-data="data1" chart-labels="label1" chart-colors="colors1" chart-options="options1">
                                </canvas> 
                            </div>
                            <div class="col-sm-4" >
                                <span style="margin-bottom:10px">Payment Type %</span>
                                <canvas style="margin-top:14px" id="pie2" class="chart chart-doughnut"
                                        chart-data="data2" chart-labels="label2" chart-colors="colors2" chart-options="options2">
                                </canvas> 

                            </div>
                            <div class="col-sm-4" >
                                <span style="margin-bottom:10px">Affiliates %</span>
                                <canvas style="margin-top:14px" id="pie3" class="chart chart-doughnut"
                                        chart-data="data3" chart-labels="label3" chart-colors="colors3" chart-options="options3">
                                </canvas> 

                            </div>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-md-12">
                                <p style="margin-bottom: -5px;">All Transaction RM</p>
                                <h3 class="counter m-b-0 col-white"><% total | number %> <span style="font-size: 14px;"></span></h3>
                                <!--<span style="float: right; margin-top: -30px;">55%</span>-->
                                <div class="progress">
                                    <div class="progress-bar l-blush" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 54.5%;"></div>
                                </div>
                            </div>

                        </div> 
                        <div class="row m-t-20">
                            <div class="col-md-12">
                                <p style="margin-bottom: -5px;">Paid Transactions RM</p>
                                <h3 class="counter m-b-0 col-white"><% paid | number %> <span style="font-size: 14px;"></span></h3>
                                <!--<span style="float: right; margin-top: -30px;">55%</span>-->
                                <div class="progress">
                                    <div class="progress-bar l-blush" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 54.5%;"></div>
                                </div>
                            </div>

                        </div> 
                        <div class="row m-t-20">
                            <div ng-controller="txnalltimeCtrl" class="col-md-4">
                                <span class="text-muted">Unique All-Time Txns</span>
                                <h3 class="counter m-b-0 col-white"><% unique_all_status %></h3>
                            </div>
                            <div ng-controller="txnalltimeCtrl" class="col-md-4">
                                <span class="text-muted">Unique All-Time Paid Txns</span>
                                <h3 class="counter m-b-0 col-white"><% unique_count %></h3>
                            </div>
                            <div class="col-sm-4">
                                <span class="text-muted">AVPT RM</span>
                                <h3 class="counter m-b-0 col-white"><% avpt %></h3>
                                <!--<span><i class="zmdi zmdi-caret-up text-success m-b-20"></i> +5%</span>-->
                            </div>
                        </div>

                    </div>
                </div>
                <div  ng-controller="merchantCtrl" class="card card-fix-margin bg-dark">
                    <div class=" card bg-dark">
                        <div class="header">
                            <h2><strong>Merchants</strong> All-time</h2>
                            <!--ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                        <li><a href="javascript:void(0);">This Year</a></li>
                                        <li><a href="javascript:void(0);">Previous Year</a></li>
                                        <li><a href="javascript:void(0);">Next Year</a></li>
                                    </ul>
                                </li>
                            </ul-->
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-6">&nbsp;</div>
                                <div class="col-sm-6" >
                                    <canvas style="margin-bottom:20px" id="pie1" class="chart chart-doughnut" style="width: 180px; height: 90px; float: right; margin-bottom: -30px; margin-top:-40px; margin-right:-50px;"
                                            chart-data="data1" chart-labels="label1" chart-colors="colors1" chart-options="options1">
                                    </canvas> 
                                </div>
                            </div>

                            <div class="row m-t-20">
                                <div class="col-sm-4 col-4 m-b-10">
                                    <span class="text-muted">Active</span>
                                    <h5 class="m-b-0"><% active %></h5>
                                </div>
                                <div class="col-sm-4 col-4 m-b-10">
                                    <p style="margin-bottom: -5px;">Total</p>
                                    <h3 class="counter m-b-0"><% all %> <span style="font-size: 14px;">/ 10,000</span></h3>
                                </div>
                                <div class="col-sm-4 col-4 m-b-10">
                                    <span class="text-muted">ARPU (RM)</span>
                                    <h5 class="m-b-0"><% arpu %></h5>
                                </div>

                            </div> 
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <span class="text-muted">Online Biz 0717</span>
                                    <h5 class="m-b-0">53,285 / <% (active/53285)*100 | number : 2 %>%</h5>
                                </div> 
                            </div> 
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <span class="text-muted">SSM 2018</span>
                                    <h5 class="m-b-0">6,859,080 / <% (active/6859080)*100 | number : 2 %>%</h5>
                                </div> 
                            </div> 
                        </div>
                    </div>
                </div>

            </div>
            <!-- SECTION 2 : SERVER ======================================= -->
            <div ng-controller="serverCtrl" class="col-lg-3 col-md-6 col-sm-12 text-center fix-col">
                <div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> <% chart_server[0]['name'] %> Memory Usage</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-muted"><i class="zmdi zmdi-circle text-success"></i> Diskspace Usage %</span>
                                <span style="float: right;"> <% chart_server[0]['disk'][(chart_server.length) - 1]['disk'] %> </span>
                                <div class="progress m-t-10 m-b-20">
                                    <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <% chart_server[0]['disk'][(chart_server.length) - 1]%>%;"></div>
                                </div>
                                <canvas id="line" class="chart chart-line" chart-data="data_memory01"
                                        chart-labels="labels_memory01" chart-series="series_memory01" chart-options="options_memory01"
                                        chart-dataset-override="datasetOverride_memory01" chart-click="onClick_memory01">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div>
                <div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> <% chart_server[0]['name'] %> CPU Usage</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-12">
                                <canvas id="line" class="chart chart-line" chart-data="data_cpu01"
                                        chart-labels="labels_cpu01" chart-series="series_cpu01" chart-options="options_cpu01"
                                        chart-dataset-override="datasetOverride_cpu01" chart-click="onClick_cpu01">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div>
                <div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> <% chart_server[1]['name'] %> Memory Usage</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-muted"><i class="zmdi zmdi-circle text-success"></i> Diskspace Usage %</span>
                                <span style="float: right;"> <% chart_server[1]['disk'][(chart_server.length) - 1]['disk'] %> </span>
                                <div class="progress m-t-10 m-b-20">
                                    <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <% chart_server[0]['disk'][(chart_server.length) - 1]%>%;"></div>
                                </div>
                                <canvas id="line" class="chart chart-line" chart-data="data_memory02"
                                        chart-labels="labels_memory02" chart-series="series_memory02" chart-options="options_memory02"
                                        chart-dataset-override="datasetOverride_memory02" chart-click="onClick_memory02">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div>
                <div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> <% chart_server[1]['name'] %> CPU Usage</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-12">
                                <canvas id="line" class="chart chart-line" chart-data="data_cpu02"
                                        chart-labels="labels_cpu02" chart-series="series_cpu02" chart-options="options_cpu02"
                                        chart-dataset-override="datasetOverride_cpu02" chart-click="onClick_cpu02">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div>

                <!-- div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> 04</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-8">
                                <span class="text-muted"><i class="zmdi zmdi-circle text-success"></i> Diskspace (33%)</span>
                                <span style="float: right;"> <% chart_server[01]['disk']['used'] %> GB of <% chart_server[0]['disk']['total'] %> GB</span>
                                <div class="progress m-t-10 m-b-20">
                                    <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <% chart_server[0]['disk']['percentage'] %>%;"></div>
                                </div>
                                <canvas id="line" class="chart chart-bar" chart-data="data"
                                        chart-labels="labels" chart-series="series" chart-options="options"
                                        chart-dataset-override="datasetOverride" chart-click="onClick">
                                </canvas>
                            </div>
                            <div class="col-md-4">
                                <canvas id="line" class="chart chart-line" chart-data="data2"
                                        chart-labels="labels2" chart-series="series2" chart-options="options2"
                                        chart-dataset-override="datasetOverride2" chart-click="onClick">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div>
                <div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> 05</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-8">
                                <span class="text-muted">Diskspace</span>
                                <span style="float: right;"> <% chart_server[01]['disk']['used'] %> GB of <% chart_server[0]['disk']['total'] %> GB</span>
                                <div class="progress m-t-10 m-b-20">
                                    <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <% chart_server[0]['disk']['percentage'] %>%;"></div>
                                </div>
                                <canvas id="line" class="chart chart-bar" chart-data="data"
                                        chart-labels="labels" chart-series="series" chart-options="options"
                                        chart-dataset-override="datasetOverride" chart-click="onClick">
                                </canvas>
                            </div>
                            <div class="col-md-4">
                                <canvas id="line" class="chart chart-line" chart-data="data2"
                                        chart-labels="labels2" chart-series="series2" chart-options="options2"
                                        chart-dataset-override="datasetOverride2" chart-click="onClick">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div>
                <div class="card server tasks_report small-margin">
                    <div class="header text-left">
                        <h2><strong>Server</strong> 06</h2>
                    </div>
                    <div class="body text-left">
                        <div class="row">
                            <div class="col-md-8">
                                <span class="text-muted">Diskspace</span>
                                <span style="float: right;"> <% chart_server[01]['disk']['used'] %> GB of <% chart_server[0]['disk']['total'] %> GB</span>
                                <div class="progress m-t-10 m-b-20">
                                    <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <% chart_server[0]['disk']['percentage'] %>%;"></div>
                                </div>
                                <canvas id="line" class="chart chart-bar" chart-data="data"
                                        chart-labels="labels" chart-series="series" chart-options="options"
                                        chart-dataset-override="datasetOverride" chart-click="onClick">
                                </canvas>
                            </div>
                            <div class="col-md-4">
                                <canvas id="line" class="chart chart-line" chart-data="data2"
                                        chart-labels="labels2" chart-series="series2" chart-options="options2"
                                        chart-dataset-override="datasetOverride2" chart-click="onClick">
                                </canvas>
                            </div>
                        </div>                 
                    </div>
                </div-->

            </div>    

            <!-- SECTION 3 & 4 : SUPPORT & WEB ======================================= -->
            <div ng-controller="zendeskCtrl"  class="col-lg-3 col-md-3 col-sm-12">
                <div class="row">
                    <div class="col-md-6 fix-col">
                        <div class="card card-fix-margin bg-dark text-center ">
                            <div class="header text-left">
                                <h2><strong>Zendesk</strong> Stats</h2>
                            </div>
                            <div class="body">  
                                <span class="text-muted">Today</span>                   
                                <hr>                        
                                <div class="row m-b-30">
                                    <div class="col-4 no-padding">
                                        <h5 class="no-margin"><%today_new%></h5>
                                        <small>New</small>
                                    </div>
                                    <div class="col-4 no-padding">
                                        <h5 class="no-margin"><%today_pending%></h5>
                                        <small>Pending</small>
                                    </div>
                                    <div class="col-4 no-padding">
                                        <h5 class="no-margin"><%today_solved%></h5>
                                        <small>Solved</small>
                                    </div>
                                </div>
                                <span class="text-muted">This Month</span>
                                <hr>                        
                                <div class="row m-b-30">
                                    <div class="col-4 no-padding">
                                        <h5 class="no-margin"><%thismonth_new%></h5>
                                        <small>New</small>
                                    </div>
                                    <div class="col-4 no-padding">
                                        <h5 class="no-margin"><%thismonth_pending%></h5>
                                        <small>Pending</small>
                                    </div>
                                    <div class="col-4 no-padding">
                                        <h5 class="no-margin"><%thismonth_solved%></h5>
                                        <small>Solved</small>
                                    </div>
                                </div>
                                <span class="text-muted">All Time</span>
                                <hr>                        
                                <div class="row m-b-30">
                                    <div class="col-6 no-padding">
                                        <h5 class="no-margin"><% new_average_daily %> </h5>
                                        <small>New Average Daily</small>
                                    </div>
                                    <div class="col-6 no-padding">
                                        <h5 class="no-margin"><% alltime_closed %></h5>
                                        <small>All-time Closed</small>
                                    </div>
                                </div>

                                <!--<div class="text-center sparkline-pie m-t-5">6,8</div>-->
                            </div>
                        </div>

                        <div class=" card card-fix-margin bg-dark">
                            <div style="color:red" class="header">
                                <h2><strong>Calls</strong></h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div style="color:red" class="col-sm-12 col-12 m-b-10 text-center">
                                        <h3 style="color:red !important" class="counter m-b-0 col-white">3,000</h3>
                                        <small class="text-muted">All Time Call Count</small>
                                        <hr>
                                    </div>
                                </div>
                                <div style="color:red" class="row">
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Today</span>
                                        <h5 class="m-b-0">30</h5>
                                        <span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">This Month</span>
                                        <h5 class="m-b-0">399</h5>
                                        <span><i class="zmdi zmdi-caret-down text-danger"></i> +5%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Average</span>
                                        <h5 class="m-b-0">28</h5>
                                        <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div style="color:red" class=" card card-fix-margin product-report">
                            <div class="header">
                                <h2 style="color:red !important"><strong>Approval</strong></h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12 col-12 m-b-10 text-center">
                                        <div style="color:red !important" class="icon l-green label-doc" style="width: 100%">Positive</div>
                                    </div>
                                </div>                     
                                <div style="color:red !important" class="row m-t-20">
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Document</span>
                                        <h5 class="m-b-0">30</h5>
                                        <span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Bank</span>
                                        <h5 class="m-b-0">399</h5>
                                        <span><i class="zmdi zmdi-caret-down text-danger"></i> +5%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Success</span>
                                        <h5 class="m-b-0">280</h5>
                                        <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>
                    <div style="color:red !important" class="col-md-6 fix-col">
                        <div class=" card card-fix-margin">
                            <div class="header">
                                <h2 style="color:red !important"><strong>Web</strong> Ranking</h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12 col-12 m-b-10 text-center">
                                        <h3 style="color:red !important" class="counter m-b-0 col-white">201,000</h3>
                                        <small style="color:red !important" class="text-muted">Web Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                    </div>
                                </div>
                                <hr>                        
                                <div style="color:red !important" class="row m-t-20">
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Web View</span>
                                        <h5 class="m-b-0">30</h5>
                                        <span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">App View</span>
                                        <h5 class="m-b-0">399</h5>
                                        <span><i class="zmdi zmdi-caret-down text-danger"></i> +5%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Avg Session</span>
                                        <h5 class="m-b-0">10 min</h5>
                                        <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                                    </div>
                                </div>                     
                                <div class="row m-t-20">
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">Bounce</span>
                                        <h5 style="color:red !important" class="m-b-0">30%</h5>
                                        <span style="color:red !important"><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">New Visitor</span>
                                        <h5 style="color:red !important" class="m-b-0">50%</h5>
                                        <span style="color:red !important"><i class="zmdi zmdi-caret-down text-danger"></i> +5%</span>
                                    </div>
                                    <div class="col-sm-4 col-4 m-b-10 text-center">
                                        <span class="text-muted">All-time Hit</span>
                                        <h3 style="color:red !important" class="m-b-0">10,291</h3>
                                    </div>
                                </div> 
                            </div>
                            <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#222"
                                 data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(0, 150, 136)" data-spot-Color="rgb(0, 188, 212)"
                                 data-offset="90" data-width="100%" data-height="40px" data-line-Width="2" data-line-Color="rgba(255, 255, 255, 0.2)"
                                 data-fill-Color="rgba(0, 0, 0, 0.1)"> 4,6,2,5,4,7,5,4,6</div>
                        </div>

                        <div style="color:red" class="card card-fix-margin">
                            <div class="header">
                                <h2><strong>Referral</strong> Stats</h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <span class="text-muted">Referral</span>
                                        <h5 style="color:red !important" class="m-b-0">56,390</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="text-muted">Direct</span>
                                        <h5 style="color:red !important" class="m-b-0">103,392</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="text-muted">Social</span>
                                        <h5 style="color:red !important" class="m-b-0">76,038</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="text-muted">Organic</span>
                                        <h5 style="color:red !important" class="m-b-0">103,392</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="text-muted">Others</span>
                                        <h5 style="color:red !important" class="m-b-0">76,038</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="m-t-30 col-sm-12">
                                        <span class="text-muted">Top Referral</span>
                                        <ol class="m-t-10" style="padding-left: 15px;">
                                            <li style="color:red !important" class="m-b-5">
                                                http://mudah.my/senangpay/
                                            </li>
                                            <li style="color:red !important" class="m-b-5">
                                                http://mdec.com.my/
                                            </li>
                                            <li style="color:red !important" class="m-b-5">
                                                http://rotikaya.my
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="m-t-30 col-sm-12">
                                        <span class="text-muted">Top Keywords</span>
                                        <ol class="m-t-10" style="padding-left: 15px;">
                                            <li style="color:red !important" class="m-b-5">
                                                online payment gateway
                                            </li>
                                            <li style="color:red !important" class="m-b-5">
                                                senangPay
                                            </li>
                                            <li style="color:red !important" class="m-b-5">
                                                billplz
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-12">
                                        <span class="text-muted">Keywords Rank</span>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td style="color:red !important">online payment gate...</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="color:red !important">online business</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="color:red !important">perniagaan online</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card card-fix-margin">
                            <div class="header">
                                <h2><strong>Social</strong> Value</h2>
                            </div>
                            <div class="body">
                            </div>
                        </div>

                    </div>
                </div>
                <!--div class="col-sm-12 fix-col fix-m--15">

                    <div class=" card card-fix-margin">
                        <div class="header">
                            <h2><strong>Social </strong>Media</h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-2 col-2 m-b-10 text-center">
                                    <h3 class="counter m-b-0 col-white">5 M</h3>
                                    <small class="text-muted">Worth</small>
                                </div>
                                <div class="col-sm-2 p-b-20">
                                    <i class="zmdi zmdi-facebook m-r-5 m-b-10" style="color:#3B5998;"></i><br>
                                    <span><i class="zmdi zmdi-accounts-alt m-r-5"></i>12,000<br>
                                        <i class="zmdi zmdi-local-offer m-r-5"></i>$ 12,000</span>
                                </div>
                                <div class="col-sm-2">
                                    <i class="zmdi zmdi-instagram m-r-5 m-b-10" style="color:#c32aa3;"></i><br>
                                    <span><i class="zmdi zmdi-accounts-alt m-r-5"></i>12,000<br>
                                        <i class="zmdi zmdi-local-offer m-r-5"></i>$ 12,000</span>
                                </div>
                                <div class="col-sm-2">
                                    <i class="zmdi zmdi-twitter m-r-5 m-b-10" style="color:#1da1f2;"></i><br>
                                    <span><i class="zmdi zmdi-accounts-alt m-r-5"></i>12,000<br>
                                        <i class="zmdi zmdi-local-offer m-r-5"></i>$ 12,000</span>
                                </div>
                                <div class="col-sm-2">
                                    <i class="zmdi zmdi-youtube-play m-r-5 m-b-10" style="color:#ff0000;"></i><br>
                                    <span><i class="zmdi zmdi-accounts-alt m-r-5"></i>12,000<br>
                                        <i class="zmdi zmdi-local-offer m-r-5"></i>$ 12,000</span>
                                </div>
                                <div class="col-sm-2">
                                    <i class="zmdi zmdi-linkedin m-r-5 m-b-10" style="color:#007bb5;"></i><br>
                                    <span><i class="zmdi zmdi-accounts-alt m-r-5"></i>12,000<br>
                                        <i class="zmdi zmdi-local-offer m-r-5"></i>$ 12,000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div -->

            </div>
            <!-- SECTION 5 & 6 : MISC ======================================= -->
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="row">
                    <div class="col-md-6 fix-col">
                        <div class="card card-fix-margin bg-dark">
                            <div class="header">
                                <h2 style="color:red !important"><strong>Ranking</strong></h2>
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else</a></li>
                                        </ul>
                                    </li>
                                    <li class="remove">
                                        <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">                      
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane in active" id="wb">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Website</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td style="color:red !important">iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td style="color:red !important">paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td style="color:red !important">senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td style="color:red !important">billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td style="color:red !important">molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td style="color:red !important">iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td style="color:red !important">eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td style="color:red !important">braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td style="color:red !important">eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div><div role="tabpanel" class="tab-pane" id="fb">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Facebook</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><div role="tabpanel" class="tab-pane" id="ig">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Instagram</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><div role="tabpanel" class="tab-pane" id="tw">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Twitter</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><div role="tabpanel" class="tab-pane" id="yt">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Youtube</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!--div role="tabpanel" class="tab-pane" id="bl">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>BL</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><div role="tabpanel" class="tab-pane" id="el">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>EL</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div--><div role="tabpanel" class="tab-pane" id="pr">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Page Ranking</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td style="color:red !important">iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td style="color:red !important">paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><div role="tabpanel" class="tab-pane" id="gt">
                                        <div class="col-sm-12 col-12 m-b-5 text-center">
                                            <span>Grand Total</span>
                                            <h3 class="counter m-b-0 col-white">3 / 12</h3>
                                            <small class="text-muted">All Time Ranking <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-caret-up" style="color: #14cd7d;"></i></span></h2></small>
                                        </div>
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>#1</td>
                                                        <td>iPay88</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#2</td>
                                                        <td>paypal</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#3</td>
                                                        <td>senangPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#4</td>
                                                        <td>billPlz</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#5</td>
                                                        <td>molPay</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>#6</td>
                                                        <td>iBill</td>
                                                        <td><span>7</span></td>
                                                        <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                    </tr>
                                                <td>#2</td>
                                                <td>eGhl</td>
                                                <td><span>7</span></td>
                                                <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#3</td>
                                                    <td>braintree</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#4</td>
                                                    <td>eway</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#5</td>
                                                    <td>worldpay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-down text-danger"></i> +18%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>#6</td>
                                                    <td>maybankPay</td>
                                                    <td><span>7</span></td>
                                                    <td><span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#wb"><i class="zmdi zmdi-globe"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#fb"><i class="zmdi zmdi-facebook"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ig"><i class="zmdi zmdi-instagram"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tw"><i class="zmdi zmdi-twitter"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#yt"><i class="zmdi zmdi-youtube-play"></i></a></li>
                                    <!--li class="nav-item"><a class="nav-link" data-toggle="tab" href="#el"><i class="zmdi zmdi-account"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#bl"><i class="zmdi zmdi-email"></i></a></li-->
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#pr"><i class="zmdi zmdi-settings"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#gt"><i class="zmdi zmdi-settings"></i></a></li>
                                </ul>   
                            </div>
                        </div>
                        <div class="card card-fix-margin bg-dark text-center ">
                            <div class="header text-left">
                                <h2 ><strong>Google </strong>Alert</h2>
                            </div>
                            <div class="body text-left">
                                <div class="row">
                                    <div class="m-t-30 col-sm-12">
                                        <h5 class="m-b-0" style="margin-top: -20px;">senangPay</h5>
                                        <ol class="m-t-10 list-unstyled">
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-twitter"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Tweet</a></span>
                                            </li>
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-twitter"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Tweet</a></span>
                                            </li>
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-facebook"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Post</a></span>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>

                            <div class="body text-left">
                                <div class="row">
                                    <div class="m-t-30 col-sm-12">
                                        <h5 class="m-b-0" style="margin-top: -35px;">payment gateway malaysia</h5>
                                        <ol class="m-t-10 list-unstyled">
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-twitter"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Tweet</a></span>
                                            </li>
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-facebook"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Post</a></span>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>

                            <div class="body text-left">
                                <div class="row">
                                    <div class="m-t-30 col-sm-12">
                                        <h5 class="m-b-0" style="margin-top: -35px;">competitor</h5>
                                        <ol class="m-t-10 list-unstyled">
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-facebook"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Post</a></span>
                                            </li>
                                            <li class="m-b-10">
                                                <i class="zmdi zmdi-facebook"></i>
                                                <strong style="color:red !important">Payment Gateway paling best!</strong><br>
                                                <span>saya guna senangPay. setakat ni okay takde masalah....</span><br>
                                                <span><a href="#">View Post</a></span>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 fix-col">
                        <!--div class="card card-fix-margin">
                            <div class="header">
                                <h2><strong>Domain </strong> worth</h2>
                            </div>
                            <div class="body text-center">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="m-t-10 m-b-0">$ 31,600</h3>
                                        <span>Estimated Value</span>
                                        <br><span><i class="zmdi zmdi-caret-up text-success"></i> +51%</span>
                                    </div>
                                    <div class="col-md-6">
                                        <h3 class="counter m-t-10 m-b-0 col-white">3</h3>
                                        <small class="text-muted">Page Rank</small>
                                        <br><span><i class="zmdi zmdi-caret-up text-success"></i> +51%</span>
                                    </div>
                                </div>
                            </div>
                        </div -->
                        <div class=" card card-fix-margin">
                            <div class="header">
                                <h2><strong>Product </strong></h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12 col-12">
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-narrow table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Item</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Payment Form</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Payment Collection</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Woocommerce</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>OpenCart</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>PrestaShop</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kedai Online</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>OnPay</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Facebook</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Instagram</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Twitter</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Wix</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Recurring</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Split Settlement</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tokenization</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>MOTO</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=" card card-fix-margin">
                            <div class="header">
                                <h2><strong>Category </strong></h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12 col-12">
                                        <div class="table-responsive m-t-10">
                                            <table class="table table-narrow table-hover m-b-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Clothing</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Services</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Finances</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Health</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Agriculture</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Computer</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Clothing</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Finance</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Services</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tourism</td>
                                                        <td style="color:red !important">56,000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script>
    /*global $ */
    $(document).ready(function () {
        "use strict";
        $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
        //Checks if li has sub (ul) and adds class for toggle icon - just an UI

        $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
        $(".menu > ul > li").hover(function (e) {
            if ($(window).width() > 943) {
                $(this).children("ul").stop(true, false).fadeToggle(150);
                e.preventDefault();
            }
        });
        //If width is more than 943px dropdowns are displayed on hover    
        $(".menu > ul > li").on('click', function () {
            if ($(window).width() <= 943) {
                $(this).children("ul").fadeToggle(150);
            }
        });
        //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

        $(".h-bars").on('click', function (e) {
            $(".menu > ul").toggleClass('show-on-mobile');
            e.preventDefault();
        });
        //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

    });
</script>    
