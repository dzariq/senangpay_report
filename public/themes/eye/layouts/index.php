﻿<?php include 'header.php';?>
<?php include 'menu.php';?>

<section class="content">
    <!-- div class="block-header" style="margin-top: -50px;">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Dashboard
                <small>Tuesday Sept 25, 2018</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> senangPay</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div -->
    <div class="container-fluid" style="margin-top: -20px;">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="card bg-dark">
                    <div class="header">
                        <h2>Transaction <strong>Summary</strong></h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">Reload Data</a></li>
                                    <li><a href="javascript:void(0);">Previous Report</a></li>
                                    <li><a href="javascript:void(0);">Next Projection</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 text-center m-b-10">                               
                                <h3 class="counter m-b-0 col-white">RM 401,516.20</h3>
                                <small class="text-muted">Value Today - 15% <i class="zmdi zmdi-trending-down" style="color: #ef6a83;"></i></small>
                                <div class="sparkline m-t-10" data-type="bar" data-width="97%" data-height="20px" data-bar-Width="2" data-bar-Spacing="6" data-bar-Color="#8f8f90">7,5,3,1,5,9,8,5,2,6,5,4,2,5,8,4,5,6,3,5,7,8</div>
                            </div>                            
                            <div class="col-lg-4 col-md-4 col-sm-4 text-center m-b-10">
                                <h3 class="counter m-b-0 col-white">RM 3,102,915.00</h3>
                                <small class="text-muted">Value This Month - 35% <i class="zmdi zmdi-trending-up" style="color: #14cd7d;"></i></small>
                                <div class="sparkline m-t-10" data-type="bar" data-width="97%" data-height="20px" data-bar-Width="2" data-bar-Spacing="6" data-bar-Color="#8f8f90">6,5,4,3,2,1,9,8,7,8,5,2,2,5,8,4,5,6,7,8,4,6</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 text-center m-b-10">
                                <h3 class="counter m-b-0 col-white">10,481 <small>(14 /min)</small></h3>
                                <small class="text-muted">Count (Frequency) - 5% <i class="zmdi zmdi-trending-up" style="color: #14cd7d;"></i></small>
                                <div class="sparkline m-t-10" data-type="bar" data-width="97%" data-height="20px" data-bar-Width="2" data-bar-Spacing="6" data-bar-Color="#26c6da">2,5,8,4,5,6,3,5,7,8,4,6,7,5,3,1,5,9,8,5,5,4</div>
                            </div>
                        </div>
                        <div id="m_area_chart2"></div>
                    </div>
                </div>
            </div><div class="col-md-12 col-lg-3">
                <div class="card">
                    <div class="header">
                        <h2>Transaction <strong>Target</strong></h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">View by Transaction</a></li>
                                    <li><a href="javascript:void(0);">View by Package</a></li>
                                    <li><a href="javascript:void(0);">View by Merchant</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Yearly Transaction Value (to date)</p>
                                <h2>RM 18,241,290.10 <span style="font-size: 14px;">/ RM 25,000,000.00</span></h2>
                            </div>
                        </div>    
                        <div class="progress">
                            <div class="progress-bar l-blush" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 54.5%;"></div>
                        </div>
                        <div class="table-responsive m-t-10">
                            <table class="table table-hover m-b-0">
                                <tbody>
                                    <tr>
                                        <td>Merchant</td>
                                        <td>Value</td>
                                        <td>Type</td>
                                        <td>Customer</td>
                                        <td>Time</td>
                                    </tr>
                                    <tr>
                                        <td>Mohd Noah b Maiden</td>
                                        <td><span>RM 199.00</span></td>
                                        <td>CC</td>
                                        <td>Luna Maya</td>
                                        <td>14:02</td>
                                    </tr>
                                    <tr>
                                        <td>Mohd Noah b Maiden</td>
                                        <td><span>RM 199.00</span></td>
                                        <td>CC</td>
                                        <td>Luna Maya</td>
                                        <td>14:02</td>
                                    </tr>
                                    <tr>
                                        <td>Mohd Noah b Maiden</td>
                                        <td><span>RM 199.00</span></td>
                                        <td>CC</td>
                                        <td>Luna Maya</td>
                                        <td>14:02</td>
                                    </tr>
                                    <tr>
                                        <td>Mohd Noah b Maiden</td>
                                        <td><span>RM 199.00</span></td>
                                        <td>CC</td>
                                        <td>Luna Maya</td>
                                        <td>14:02</td>
                                    </tr>
                                    <tr>
                                        <td>Mohd Noah b Maiden</td>
                                        <td><span>RM 199.00</span></td>
                                        <td>CC</td>
                                        <td>Luna Maya</td>
                                        <td>14:02</td>
                                    </tr>
                                    <tr>
                                        <td>Mohd Noah b Maiden</td>
                                        <td><span>RM 199.00</span></td>
                                        <td>CC</td>
                                        <td>Luna Maya</td>
                                        <td>14:02</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-6 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Type</strong> of transaction</h2>
                    </div>
                    <div class="body text-center" style="margin-top: -25px;">
                        <div class="text-center sparkline-pie">4,8</div>

                        <div class="m-t-20 text-center sparkline-pie">6,4,8</div>

                        <div class="m-t-20 text-center sparkline-pie">6,4</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
                <div class="card ">
                    <div class="header">
                        <h2><strong>Sign up</strong> counts</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Value</p>
                                <h2>RM 30,000.00 <span style="font-size: 14px;"> + 15% <i class="zmdi zmdi-trending-up" style="color: #14cd7d;"></i></span></h2>
                            </div>
                            <div class="col-sm-3 col-3 m-b-10">
                                <span class="text-muted">Today</span>
                                <h5 class="m-b-0">6</h5>
                                <span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                            </div>
                            <div class="col-sm-3 col-3 m-b-10">
                                <span class="text-muted">This Month</span>
                                <h5 class="m-b-0">280</h5>
                                <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                            </div>
                            <div class="col-sm-3 col-3 m-b-10">
                                <span class="text-muted">Affiliate</span>
                                <h5 class="m-b-0">6</h5>
                                <span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                            </div>
                            <div class="col-sm-3 col-3 m-b-10">
                                <span class="text-muted">Renewal</span>
                                <h5 class="m-b-0">54</h5>
                                <span><i class="zmdi zmdi-caret-down text-danger"></i> -5%</span>
                            </div>
                        </div> 

                    </div>
                    <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(63, 81, 181)" data-highlight-Line-Color="#222"
                    data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(63, 81, 181)" data-spot-Color="rgb(63, 81, 181, 0.7)"
                    data-offset="90" data-width="100%" data-height="50px" data-line-Width="1" data-line-Color="rgb(63, 81, 181, 0.7)"
                    data-fill-Color="rgba(221,94,137, 0.7)"> 1,2,3,1,4,3,6,4,4,1 </div>
                </div>

                <div class="card ">
                    <div class="header">
                        <h2><strong>Merchants</strong> counts</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-sm-4 col-4 m-b-10">
                                <span class="text-muted">Active</span>
                                <h5 class="m-b-0">3435</h5>
                                <span><i class="zmdi zmdi-caret-up text-success"></i> +18%</span>
                            </div>
                            <div class="col-sm-4 col-4 m-b-10">
                                <span class="text-muted">Total</span>
                                <h5 class="m-b-0">5450</h5>
                                <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                            </div>
                            <div class="col-sm-4 col-4 m-b-10">
                                <span class="text-muted">Target</span>
                                <h5 class="m-b-0">10,000</h5>
                                <div class="progress m-t-10">
                                    <div class="progress-bar l-blush" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 54.5%;"></div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-6 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Unique</strong> users</h2>
                    </div>
                    <div class="body text-center">
                        <div class="sparkline m-b-20" data-type="bar" data-width="97%" data-height="40px" data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#00ced1">10,12,12,13,13,14,14,10,6,6,7,9,11,14,14,13,13,12,12,11</div>
                        <h3 class="m-b-0">10,400</h3>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>Transaction</strong></h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">Packages</a></li>
                                    <li><a href="javascript:void(0);">Affiliates</a></li>
                                    <li><a href="javascript:void(0);">Active</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <span class="text-muted">Average Value</span>
                        <h3 class="m-b-0">RM 40.00</h3>
                        <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                        <br>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>AVPT</strong></h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">Packages</a></li>
                                    <li><a href="javascript:void(0);">Affiliates</a></li>
                                    <li><a href="javascript:void(0);">Active</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <span class="text-muted">Average Value</span>
                        <h3 class="m-b-0">RM 40.00</h3>
                        <span><i class="zmdi zmdi-caret-up text-success"></i> +5%</span>
                        <br>
                    </div>
                </div>
            </div>
         </div>

         <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <div id="real_time_chart" class="flot-chart"></div>                      
                        <h6 class="m-t-20">Mailserver</h6>                      
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <div class="sales-bars-chart" style="height: 320px;"> </div>
                        <h6 class="m-t-20">Apps</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <div id="tracking_chart" class="flot-chart"></div>
                        <h6 class="m-t-20">Web</h6>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <div id="multiple_axis_chart" class="flot-chart"></div>
                        <h6 class="m-t-20">Backup</h6>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="col-sm-12 col-md-5 col-lg-3">
                <div class="card member-card">
                    <div class="header l-coral">
                        <h4 class="m-t-10">Eliana Smith</h4>
                    </div>
                    <div class="member-img">
                        <a href="profile.html" class="">
                        <img src="../assets/images/lg/avatar2.jpg" class="rounded-circle" alt="profile-image">
                        </a>
                    </div>
                    <div class="body">
                        <div class="col-12">
                            <ul class="social-links list-unstyled">
                                <li>
                                <a title="facebook" href="#">
                                    <i class="zmdi zmdi-facebook"></i>
                                </a>
                                </li>
                                <li>
                                <a title="twitter" href="#">
                                    <i class="zmdi zmdi-twitter"></i>
                                </a>
                                </li>
                                <li>
                                <a title="instagram" href="3">
                                    <i class="zmdi zmdi-instagram"></i>
                                </a>
                                </li>
                            </ul>
                            <p class="text-muted">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p>
                        </div>                        
                        <hr>                        
                        <div class="row">
                            <div class="col-4">
                                <h5>18</h5>
                                <small>Files</small>
                            </div>
                            <div class="col-4">
                                <h5>2GB</h5>
                                <small>Used</small>
                            </div>
                            <div class="col-4">
                                <h5>65,6$</h5>
                                <small>Spent</small>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#222"
                    data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(0, 150, 136)" data-spot-Color="rgb(0, 188, 212)"
                    data-offset="90" data-width="100%" data-height="40px" data-line-Width="2" data-line-Color="rgba(255, 255, 255, 0.2)"
                    data-fill-Color="rgba(0, 0, 0, 0.1)"> 1,6,2,8,4,7,3,6,2</div>
                </div>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-9">
                <div class="card">
                    <div class="header">
                        <h2><strong>Social</strong> Media <small>Lorem Ipsum is simply dummy text of the printing</small></h2>                        
                    </div>
                    <div class="body">
                        <div class="table-responsive social_media_table">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Media</th>
                                        <th>Name</th>
                                        <th>Like</th>                                                                                
                                        <th>Comments</th>
                                        <th>Share</th>
                                        <th>Members</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="social_icon linkedin"><i class="zmdi zmdi-linkedin"></i></span>
                                        </td>
                                        <td><span class="list-name">Linked In</span>
                                            <span class="text-muted">Florida, United States</span>
                                        </td>
                                        <td>19K</td>
                                        <td>14K</td>
                                        <td>10K</td>
                                        <td>
                                            <span class="badge badge-success">2341</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon twitter-table"><i class="zmdi zmdi-twitter"></i></span>
                                        </td>
                                        <td><span class="list-name">Twitter</span>
                                            <span class="text-muted">Arkansas, United States</span>
                                        </td>
                                        <td>7K</td>
                                        <td>11K</td>
                                        <td>21K</td>
                                        <td>
                                            <span class="badge badge-success">952</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon facebook"><i class="zmdi zmdi-facebook"></i></span>
                                        </td>
                                        <td><span class="list-name">Facebook</span>
                                            <span class="text-muted">Illunois, United States</span>
                                        </td>
                                        <td>15K</td>
                                        <td>18K</td>
                                        <td>8K</td>
                                        <td>
                                            <span class="badge badge-success">6127</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon google"><i class="zmdi zmdi-google-plus"></i></span>
                                        </td>
                                        <td><span class="list-name">Google Plus</span>
                                            <span class="text-muted">Arizona, United States</span>
                                        </td>
                                        <td>15K</td>
                                        <td>18K</td>
                                        <td>154</td>
                                        <td>
                                            <span class="badge badge-success">325</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon youtube"><i class="zmdi zmdi-youtube-play"></i></span>
                                        </td>
                                        <td><span class="list-name">YouTube</span>
                                            <span class="text-muted">Alaska, United States</span>
                                        </td>
                                        <td>15K</td>
                                        <td>18K</td>
                                        <td>200</td>
                                        <td>
                                            <span class="badge badge-success">160</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card agent">
                    <div class="agent-avatar"> <a href="javascript:void(0);"> <img src="../assets/images/lg/avatar1.jpg" class="img-fluid " alt=""> </a> </div>
                    <div class="agent-content">
                        <div class="agent-name">
                            <h4><a href="javascript:void(0);">Tim Hank</a></h4>
                            <span>Fairview, Texas</span>
                            <ul class="list-unstyled team-info m-b-0">
                                <li class="m-r-15"><small>Team</small></li>                                
                                <li><img src="../assets/images/xs/avatar8.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar9.jpg" alt="Avatar"></li>                            
                            </ul>
                        </div>
                        <ul class="agent-contact-details">
                            <li><i class="zmdi zmdi-phone"></i><span>(123) 123-456</span></li>
                            <li><i class="zmdi zmdi-email"></i>info@example.com</li>
                        </ul>
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a class="gplus" href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
             <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card agent">
                    <div class="agent-avatar"> <a href="javascript:void(0);"> <img src="../assets/images/lg/avatar2.jpg" class="img-fluid " alt=""> </a> </div>
                    <div class="agent-content">
                        <div class="agent-name">
                            <h4><a href="javascript:void(0);">Gary Camara</a></h4>
                            <span>Bristol, Pennsylvania</span>
                            <ul class="list-unstyled team-info m-b-0">
                                <li class="m-r-15"><small>Team</small></li>
                                <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>                            
                            </ul>
                        </div>
                        <ul class="agent-contact-details">
                            <li><i class="zmdi zmdi-phone"></i><span>(123) 123-456</span></li>
                            <li><i class="zmdi zmdi-email"></i>info@example.com</li>
                        </ul>
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a class="gplus" href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
             <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card agent">
                    <div class="agent-avatar"> <a href="javascript:void(0);"> <img src="../assets/images/lg/avatar3.jpg" class="img-fluid " alt=""> </a> </div>
                    <div class="agent-content">
                        <div class="agent-name">
                            <h4><a href="javascript:void(0);">Hossein Shams</a></h4>
                            <span>Springfield, Florida</span>
                            <ul class="list-unstyled team-info m-b-0">
                                <li class="m-r-15"><small>Team</small></li>
                                <li><img src="../assets/images/xs/avatar5.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar6.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar7.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                            </ul>
                        </div>
                        <ul class="agent-contact-details">
                            <li><i class="zmdi zmdi-phone"></i><span>(123) 123-456</span></li>
                            <li><i class="zmdi zmdi-email"></i>info@example.com</li>
                        </ul>
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a class="gplus" href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
             <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card agent">
                    <div class="agent-avatar"> <a href="javascript:void(0);"> <img src="../assets/images/lg/avatar4.jpg" class="img-fluid " alt=""> </a> </div>
                    <div class="agent-content">
                        <div class="agent-name">
                            <h4><a href="javascript:void(0);">Tom Wilson</a></h4>
                            <span>Washington, Illinois</span>
                            <ul class="list-unstyled team-info m-b-0">
                                <li class="m-r-15"><small>Team</small></li>                               
                                <li><img src="../assets/images/xs/avatar7.jpg" alt="Avatar"></li>
                                <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                            </ul>
                        </div>
                        <ul class="agent-contact-details">
                            <li><i class="zmdi zmdi-phone"></i><span>(123) 123-456</span></li>
                            <li><i class="zmdi zmdi-email"></i>info@example.com</li>
                        </ul>
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a class="gplus" href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
           <div class="col-lg-12 col-md-12">
               <div class="card ">
                   <div class="header">
                       <h2><strong>Sales</strong> Overview</h2>
                       <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                   </div>
                   <div class="body table-responsive">
                       <table class="table table-hover table-borderless m-b-0">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th style="width:40%;">Product</th>
                                   <th class="number">Price</th>
                                   <th style="width:20%;">Date</th>
                                   <th style="width:20%;">Status</th>
                                   <th style="width:5%;" class="actions"></th>
                               </tr>
                           </thead>
                           <tbody class="no-border-x">
                               <tr>
                                   <td>1</td>
                                   <td>Sony Xperia M4</td>
                                   <td class="number">$149</td>
                                   <td>Aug 23, 2017</td>
                                   <td><span class="badge badge-success">Completed</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>2</td>
                                   <td>Apple iPhone 6</td>
                                   <td class="number">$535</td>
                                   <td>Aug 20, 2017</td>
                                   <td><span class="badge badge-success">Completed</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>3</td>
                                   <td>Samsung Galaxy S7</td>
                                   <td class="number">$583</td>
                                   <td>Aug 18, 2017</td>
                                   <td><span class="badge badge-warning">Pending</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>4</td>
                                   <td>HTC One M9</td>
                                   <td class="number">$350</td>
                                   <td>Aug 15, 2017</td>
                                   <td><span class="badge badge-warning">Pending</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>5</td>
                                   <td>Sony Xperia Z5</td>
                                   <td class="number">$495</td>
                                   <td>Aug 13, 2017</td>
                                   <td><span class="badge badge-danger">Cancelled</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>6</td>
                                   <td>Samsung Galaxy S9</td>
                                   <td class="number">$583</td>
                                   <td>Aug 18, 2017</td>
                                   <td><span class="badge badge-warning">Pending</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>7</td>
                                   <td>HTC One M13</td>
                                   <td class="number">$350</td>
                                   <td>Aug 15, 2017</td>
                                   <td><span class="badge badge-warning">Pending</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>
                               <tr>
                                   <td>8</td>
                                   <td>Sony Xperia M4</td>
                                   <td class="number">$149</td>
                                   <td>Aug 23, 2017</td>
                                   <td><span class="badge badge-success">Completed</span></td>
                                   <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-plus-circle-o"></i></a></td>
                               </tr>                               
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>           
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <p class="m-b-0">© 2018 senangPay</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->

<script src="assets/bundles/flotchartsscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script src="assets/js/pages/charts/flot.js"></script>

<script src="assets/js/pages/charts/sparkline.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/charts/jquery-knob.js"></script>
<script src="assets/js/pages/index2.js"></script>
<script>
    /*global $ */
    $(document).ready(function() {
      "use strict";
      $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
      //Checks if li has sub (ul) and adds class for toggle icon - just an UI
    
      $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    
      $(".menu > ul > li").hover(function(e) {
        if ($(window).width() > 943) {
          $(this).children("ul").stop(true, false).fadeToggle(150);
          e.preventDefault();
        }
      });
      //If width is more than 943px dropdowns are displayed on hover    
      $(".menu > ul > li").on('click',function() {
        if ($(window).width() <= 943) {
          $(this).children("ul").fadeToggle(150);
        }
      });
      //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)
    
      $(".h-bars").on('click',function(e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
      });
      //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)
    
    });    
    </script>    
</body>
</html>