<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Inherit from another theme
      |--------------------------------------------------------------------------
      |
      | Set up inherit from another if the file is not exists,
      | this is work with "layouts", "partials", "views" and "widgets"
      |
      | [Notice] assets cannot inherit.
      |
     */

    'inherit' => null, //default

    /*
      |--------------------------------------------------------------------------
      | Listener from events
      |--------------------------------------------------------------------------
      |
      | You can hook a theme when event fired on activities
      | this is cool feature to set up a title, meta, default styles and scripts.
      |
      | [Notice] these event can be override by package config.
      |
     */
    'events' => array(
        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            $theme->set('title', 'Laravel Starter CMS');
            $theme->set('keywords', 'Laravel Starter CMS');
            $theme->set('description', 'Laravel Starter CMS');
        },
        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // ie9 compatibility
            $theme->asset()->container('ie9')->usePath()->add('html5shiv', 'js/ie/html5shiv.js');
            $theme->asset()->container('ie9')->usePath()->add('respond', 'js/ie/respond.min.js');
            $theme->asset()->container('ie9')->usePath()->add('excanvas', 'js/ie/excanvas.js');
            $theme->asset()->usePath()->add('bootstrap', 'plugins/bootstrap/css/bootstrap.min.css');
            $theme->asset()->usePath()->add('morris', 'plugins/morrisjs/morris.css');
            $theme->asset()->usePath()->add('main', 'css/main.css');
            $theme->asset()->usePath()->add('hm', 'css/hm-style.css');
            $theme->asset()->usePath()->add('custom', 'css/custom_sp.css');
            $theme->asset()->usePath()->add('skin', 'css/color_skins.css');
            $theme->asset()->usePath()->add('select', 'plugins/bootstrap-select/css/bootstrap-select.css');
            // scripts
            $theme->asset()->usePath()->add('libscripts', 'bundles/libscripts.bundle.js');
            $theme->asset()->usePath()->add('vendorscripts', 'bundles/vendorscripts.bundle.js');
            $theme->asset()->usePath()->add('knob', 'bundles/knob.bundle.js');
            $theme->asset()->usePath()->add('laravelmainscripts8', 'bundles/mainscripts.bundle.js');

        },
        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(
            'main' => function($theme)
            {
                
            },
            'error' => function($theme)
            {
                
            }
        )
    )
);
