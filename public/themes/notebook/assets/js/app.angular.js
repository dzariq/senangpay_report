var reportApp = angular.module('report', ['chart.js'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

reportApp.controller('dashboardCtrl', function ($scope) {
    $scope.firstName = "John";
    $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
    $scope.data = [300, 500, 100];

});