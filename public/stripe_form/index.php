<?php
include 'var.php';
# to solve issue of incomplete or no cache-control and pragma http header set
header('Cache-Control: no-cache, no-store, must-revalidate'); # HTTP 1.1.
header('Pragma: no-cache'); # HTTP 1.0.
header('Expires: 0'); # Proxies.
# to solve issue of clickjacking
header('X-Frame-Options: SAMEORIGIN');
# to solve web browser xss protection not enabled
header('X-XSS-Protection: 1; mode=block;');
# to solve issue x-content-type-options header missing
header('X-Content-Type-Options: nosniff');
# only allow content from these sources
header(APP_CSP);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo APP_HEADER; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="Online Payment Gateway">
        <meta name="author" content="">
        <meta name="theme-color" content="#192f70">
        <meta name="apple-mobile-web-app-status-bar-style" content="#192f70">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/todc-bootstrap/3.3.7-3.3.13/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL ?>/css/form.css?v=15">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL ?>/css/stripe_form.css?v=4">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/todc-bootstrap/3.3.7-3.3.13/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="sp-bar" align="center">
            <a href="<?php echo APP_WEB; ?>"><img src="<?php echo PUBLIC_URL ?>/images/logo_senangpay_v3.png"></a>
        </div>

        <div class="container-fluid">
            <div class="row shop-detail">
                <div class="col-xs-12 col-sm-9">
                    <table>
                        <tr>
                            <td class="crop">
                                <img src="<?php echo $data['header_image']; ?>" alt="">
                            </td>
                            <td style="padding-left: 5px;">
                                <h3 style="margin-top: 2px;">
                                    <?php
                                    if ($data['business_registration_name'] != '' && strtolower(html_entity_decode($data['header_name'])) != strtolower(html_entity_decode($data['business_registration_name'])))
                                    {
                                        ?>
                                        <h3 style="margin-top: 2px;"><?php echo $data['header_name']; ?><span style="text-size: 8px; color: #666;"> | <?php echo $data['business_registration_name']; ?></span></h3>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <h3 style="margin-top: 2px;"><?php echo $data['header_name']; ?><span style="text-size: 8px; color: #666;"></span></h3>
                                    <?php } ?>                 
                                    <?php if ($data['business_registration'] != ''): ?>
                                        <p style="margin-top: 5px;" class="contact-details">
                                            <?php
                                            if ($data['business_registration_no'] == '')
                                            {
                                                ?>
                                                <?php if ($data['business_registration'] != '') echo 'SSM ' . $data['business_registration']; ?>
                                                <?php
                                            }else
                                            {
                                                ?>
                                                <?php echo 'SSM ' . $data['business_registration_no']; ?>
                                            <?php } ?>
                                        </p>
                                    <?php endif; ?>
                                    <p class="contact-details"><a href="mailto:<?php echo $data['header_email']; ?>"><?php echo $data['header_email']; ?></a> | <?php echo $data['header_phone']; ?></p>
                                    <?php
                                    if ($data['sst_no'] != '')
                                    {
                                        ?>
                                        <p class="contact-details"><b>SST No. : </b><?php echo $data['sst_no']; ?></p>
                                    <?php } ?>
                                    <div id="shop-social">
                                        <ul>
                                            <?php if ($data['header_facebook'] != ''): ?>
                                                <li><a href="<?php echo $data['header_facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ($data['header_instagram'] != ''): ?>
                                                <li><a href="<?php echo $data['header_instagram']; ?>"><i class="fa fa-instagram" style="margin-left: -1px;"></i></li>
                                            <?php endif; ?>
                                            <?php if ($data['header_blog'] != ''): ?>
                                                <li><a href="<?php echo $data['header_blog']; ?>"><i class="fa fa-home" style="margin-left: -1px;"></i></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <!-- Start form section -->
            <div class="row shop-detail">
                <div class="col-md-12  fix-mobile">
                    <!-- Summary section -->
                    <div class="order-detail">
                        <h3><?php echo APP_LANG_PAYMENT_98; ?></h3>

                        <table id="cart-list">
                            <tbody>
                                <tr class="cart-item-row">
                                    <td style="width: 70%" class="item">
                                        <span class="item-name"><?php echo $data['detail']; ?></span>
                                    </td>
                                    <td style="width: 30%" class="hargasum">
                                        <?php echo $data['currency'] ?> <span class="item-price"><?php echo number_format($data['amount'], 2); ?></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="summary">
                            <table style="width: 100%">
                                <thead>
                                    <tr>
                                        <td style="width: 50%"></td>
                                        <td style="width: 50%; text-align: right"></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td><?php echo APP_LANG_PAYMENT_56; ?></td>
                                    <td style="text-align: right"><?php echo $data['currency'] ?> <span id="col_summary_total_price"><?php echo number_format($data['amount'], 2); ?></span></td>
                                </tr>
                                <tr class="border-up">
                                    <td><?php echo APP_LANG_PAYMENT_167; ?></td>
                                    <td style="text-align: right; font-size: 18px;"><?php echo $data['currency'] ?> <span class="summary_grand_total"><?php echo number_format($data['amount'], 2); ?></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End summary section -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Payment method section -->
                    <div class="order-detail">
                        <h3>
                            Choose Payment Method                    </h3>
                        <div class="choose-type">
                            <ul>


                                <!-- FOR MULTIPLE METHOD (LOOP) -->
                                <!-- SET DEFAULT PAYMENT METHOD -->
                                <input type="hidden" name="payment_mode" value="stripe" id="payment_mode">
                                <li class="active " >
                                    <a href="#stripe" data-toggle="tab" onclick="select_payment_mode('stripe')" class='stripe'><img src="<?php echo PUBLIC_URL ?>img/logo-stripe@2x.png"></a>
                                </li>
                                <?php if(in_array('fpx',$data['stripe_other_payment_methods'])){ ?>
                                <li class=" ">
                                    <a href="#fpx" data-toggle="tab" onclick="select_payment_mode('fpx')" class='fpx'><img src="<?php echo PUBLIC_URL ?>img/fpx-mobile@2x.png"></a>
                                </li>
                                <?php } ?>
                                <?php if(in_array('grabpay',$data['stripe_other_payment_methods'])){ ?>
                                <li class=" ">
                                    <a href="#grabpay" data-toggle="tab" onclick="select_payment_mode('grabpay')" class='grab'><img src="<?php echo PUBLIC_URL ?>img/grab-mobile@2x.png"></a>
                                </li>
                                <?php } ?>
                                <?php if(in_array('boost',$data['stripe_other_payment_methods'])){ ?>
                                <li class=" ">
                                    <a href="#boost" data-toggle="tab" onclick="select_payment_mode('boost')" class='boost'><img src="<?php echo PUBLIC_URL ?>img/boost-mobile@2x.png"></a>
                                </li>
                                <?php } ?>
                                <?php if(in_array('tng',$data['stripe_other_payment_methods'])){ ?>
                                <li class=" ">
                                    <a href="#tng" data-toggle="tab" onclick="select_payment_mode('tng')" class='tng'><img src="<?php echo PUBLIC_URL ?>img/tng-mobile@2x.png"></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <form id="payment-form">
                            <div class="tab-content">
                                <div class="tab-pane active" id="stripe">
                                    <div id="card-element">
                                        <!-- Elements will create input elements here -->
                                    </div>
                                    <div id="card-errors" role="alert"></div>
                                    <div id="disclaimer">
                                        <p class="footer-text">
                                            <i class="fa fa-info" style="margin-left: 15px; margin-right: 5px;"></i>
                                            Please disable any pop-up blocker</p>
                                        <p class="footer-text">
                                            <i class="fa fa-info" style="margin-left: 15px; margin-right: 5px;"></i>
                                            Yes, I agree and I have read all the <a href="https://app.senangpay.my/terms" target="_blank">Terms & Conditions</a></p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="fpx">
                                    <div class="row">
                                        <div class="rdbank m-t-20">
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(22)" type="radio" value="22" id="fpx-bank-22" name="bank_fpx" >
                                                <label for="fpx-bank-22">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_affin.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(27)" type="radio" value="27" id="fpx-bank-27" name="bank_fpx" >
                                                <label for="fpx-bank-27">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_alliance.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(13)" type="radio" value="13" id="fpx-bank-13" name="bank_fpx" >
                                                <label for="fpx-bank-13">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_ambank.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input  onclick="set_bank(3)" type="radio" value="3" id="fpx-bank-3" name="bank_fpx" >
                                                <label for="fpx-bank-3">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_bankislam.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(21)" type="radio" value="21" id="fpx-bank-21" name="bank_fpx" >
                                                <label for="fpx-bank-21">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_bankrakyat.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(25)" type="radio" value="25" id="fpx-bank-25" name="bank_fpx" >
                                                <label for="fpx-bank-25">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_bankmuamalat.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(26)" type="radio" value="26" id="fpx-bank-26" name="bank_fpx" >
                                                <label for="fpx-bank-26">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_bsn.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(2)" type="radio" value="2" id="fpx-bank-2" name="bank_fpx" >
                                                <label for="fpx-bank-2">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_cimb2.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(4)" type="radio" value="4" id="fpx-bank-4" name="bank_fpx" >
                                                <label for="fpx-bank-4">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_hongleong2.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(6)" type="radio" value="6" id="fpx-bank-6" name="bank_fpx" >
                                                <label for="fpx-bank-6">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_hsbc.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(15)" type="radio" value="15" id="fpx-bank-15" name="bank_fpx" >
                                                <label for="fpx-bank-15">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_kuwait_finance.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(29)" type="radio" value="29" id="fpx-bank-29" name="bank_fpx" >
                                                <label for="fpx-bank-29">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_maybank2e.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(7)" type="radio" value="7" id="fpx-bank-7" name="bank_fpx" >
                                                <label for="fpx-bank-7">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_maybank.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(23)" type="radio" value="23" id="fpx-bank-23" name="bank_fpx" >
                                                <label for="fpx-bank-23">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_ocbc.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(9)" type="radio" value="9" id="fpx-bank-9" name="bank_fpx" >
                                                <label for="fpx-bank-9">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_publicbank2.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(10)" type="radio" value="10" id="fpx-bank-10" name="bank_fpx" >
                                                <label for="fpx-bank-10">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_rhb2.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(24)" type="radio" value="24" id="fpx-bank-24" name="bank_fpx" >
                                                <label for="fpx-bank-24">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_standardchartered.png" alt="">
                                                </label>
                                            </div>
                                            <div class="rdbank col-xs-6 col-sm-3" >
                                                <input onclick="set_bank(14)" type="radio" value="14" id="fpx-bank-14" name="bank_fpx" >
                                                <label for="fpx-bank-14">
                                                    <img  class="bank" src="<?php echo PUBLIC_URL ?>images/bank/logo_uob.png" alt="">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="disclaimer">
                                        <p class="footer-text">
                                            <i class="fa fa-info" style="margin-left: 15px; margin-right: 5px;"></i>
                                            Please disable any pop-up blocker                                        </p>
                                        <p class="footer-text">
                                            <i class="fa fa-info" style="margin-left: 15px; margin-right: 5px;"></i>
                                            Yes, I agree and I have read all the <a href="https://app.senangpay.my/terms" target="_blank">Terms & Conditions</a>                                        </p>
                                    </div>

                                </div>
                                <div class="tab-pane" id="grabpay">
                                    <div class="summary " style="padding: 20px; text-align: center;">
                                        You will be redirected to GrabPay payment page to complete your payment. Click on the green button Pay below to continue.
                                    </div>

                                </div>
                                <div class="tab-pane" id="boost">
                                    <div class="summary " style="padding: 20px; text-align: center;">
                                        You will be redirected to Boost payment page to complete your payment. Click on the green button Pay below to continue.
                                    </div>

                                </div>
                                <div class="tab-pane" id="tng">
                                    <div class="summary " style="padding: 20px; text-align: center;">
                                        You will be redirected to Touch n Go payment page to complete your payment. Click on the green button Pay below to continue.
                                    </div>

                                </div>
                                <button class="btn-pay" id="submit" >Pay <?php echo $data['currency'] ?> <span class="col_summary_grand_total"><?php echo number_format($data['amount'], 2); ?></span></button>

                            </div>
                        </form>

                        <div class="col-md-12 text-center" style="margin-top: 40px;">
                            <img class="footer-img" src="<?php echo PUBLIC_URL ?>images/footer-img.png"><br>
                            <p class="footer-text">By proceeding, you agree to authorise senangPay (Simplepay Gateway Sdn Bhd) to debit the above net charges to your credit/debit card or online banking account.</p>
                        </div>

                    </div>
                    <!-- End payment method seciton -->
                </div>
            </div>

            <!-- Error Modal -->
            <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header alert alert-danger">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="errorModalLabel"><?php echo APP_LANG_PAYMENT_106; ?></h4>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo PUBLIC_URL ?>/js/site_level.min.js?v=7"></script>
        <script src="https://js.stripe.com/v3/"></script>

        <?php
# google analytics
    //    global $slm_directory;
     //   include_once($slm_directory['template'] . 'google_analytics.php');
        ?>
    </body>
</html>
<script>
    var bank_id = 0;
    function set_bank(id)
    {
        bank_id = id;
    }
    // Set your publishable key: remember to change this to your live publishable key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
    var stripe = Stripe('<?php echo $data['publishable_key'] ?>');
    var elements = stripe.elements();

    var style = {
        base: {
            color: "#32325d",
            fontFamily: '"Muli", sans-serif',
        }
    };

    var card = elements.create("card", {style: style});
    card.mount("#card-element");

    card.on('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    var form = document.getElementById('payment-form');

    form.addEventListener('submit', function (ev) {
        document.getElementById("submit").disabled = true;

        var payment_mode = document.getElementById("payment_mode").value;
        ev.preventDefault();

        if (payment_mode == 'stripe')
        {
            if (card._empty)
                return false;
            stripe.createPaymentMethod({
                type: 'card',
                card: card,
                billing_details: {
                    name: '<?php echo $data['name'] ?>',
                },
            })
                    .then(function (result) {
                        //create payment intent using payment method id in result object
                        $.post('/stripe/charges',
                                {
                                    transaction_reference: "<?php echo $data['transaction_reference'] ?>",
                                    payment_method: result.paymentMethod,
                                    card_details: result.paymentMethod.card
                                },
                                function (output) {
                                    var result = JSON.parse(output)
                                    if (!result.result) {
                                        var displayError = document.getElementById('card-errors');
                                        displayError.textContent = result.msg;
                                        document.getElementById("submit").disabled = false;
                                        return false;
                                    }
                                    stripe.confirmCardPayment(result.client_secret, {
                                        payment_method: {
                                            card: card,
                                            billing_details: {
                                                name: '<?php echo $data['name'] ?>'
                                            }
                                        }
                                    }).then(function (result) {
                                        if (result.error) {
                                            window.location = '<?php echo BASE_URL ?>stripe/redirect/<?php echo $data['transaction_reference'] ?>'
                                                                                    } else {
                                                                                        window.location = '<?php echo BASE_URL ?>stripe/redirect/<?php echo $data['transaction_reference'] ?>'
                                                                                                                                }
                                                                                                                            });
                                                                                                                        });
                                                                                                            });
                                                                                                } else if (payment_mode == 'boost') {
                                                                                                    $.get('/boost/payment/<?php echo $data['transaction_reference'] ?>',
                                                                                                            function (output) {
                                                                                                                var output_json = JSON.parse(output);
                                                                                                                $("#redirect-form").attr("action", output_json.checkoutURI);

                                                                                                                $("#redirect-form").submit();
                                                                                                            });
                                                                                                } else if (payment_mode == 'tng') {
                                                                                                    $.get('/tng/payment/<?php echo $data['transaction_reference'] ?>',
                                                                                                            function (output) {
                                                                                                                var output_json = JSON.parse(output);
                                                                                                                $("#redirect-form").attr("action", output_json.checkoutURI);

                                                                                                                $("#redirect-form").submit();
                                                                                                            });
                                                                                                } else if (payment_mode == 'fpx') {
                                                                                                    window.location = '/fpx/payment/<?php echo $data['transaction_reference'] ?>/' + bank_id
                                                                                                } else if (payment_mode == 'grabpay') {
                                                                                                    $.get('/grabpay/payment/<?php echo $data['transaction_reference'] ?>',
                                                                                                            function (output) {
                                                                                                                var output_json = JSON.parse(output);
                                                                                                                $("#redirect-form").attr("action", output_json.checkoutURI);

                                                                                                                $("#redirect-form").submit();
                                                                                                            });
                                                                                                }
                                                                                            });
</script>
