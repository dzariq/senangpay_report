<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('APP_HEADER', 'Senangpay');
define('PUBLIC_URL', 'https://report.senangpay.my/stripe_form/');

$gst_rate = 0;
define('APP', 'senangpay');
define('APP_COMPANY_NAME', 'senangpay');
define('APP_FRAUD_SUSPECT_URL', 'senangpay');
define('APP_CSP', "Content-Security-Policy: script-src 'self' 'unsafe-inline' www.google-analytics.com code.jquery.com smarticon.geotrust.com cdnjs.cloudflare.com https:; object-src 'self';");
define('APP_TERMS_AND_CONDITIONS_URL', 'https://report.senangpay.my');
define('APP_WEB', 'https://report.senangpay.my');
define('BASE_URL', 'https://app.senangpay.my');
define('APP_LANG_PAYMENT_1', 'Your Purchase Item');
define('APP_LANG_PAYMENT_2', 'Description');
define('APP_LANG_PAYMENT_3', 'Code');
define('APP_LANG_PAYMENT_4', 'Price');
define('APP_LANG_PAYMENT_5', 'Delivery Charges');
define('APP_LANG_PAYMENT_6', '*GST ' . $gst_rate . '% will be charged for product and delivery charges');
define('APP_LANG_PAYMENT_7', 'Purchase Information');
define('APP_LANG_PAYMENT_8', 'Quantity');
define('APP_LANG_PAYMENT_9', 'Total Product Price (RM)');
define('APP_LANG_PAYMENT_10', 'Delivery Charge (RM)');
define('APP_LANG_PAYMENT_11', 'GST (RM)');
define('APP_LANG_PAYMENT_12', 'Grand Total (RM)');
define('APP_LANG_PAYMENT_13', 'Note to seller (if any)');
define('APP_LANG_PAYMENT_14', 'Note for seller. Maximum number of characters are 100.');
define('APP_LANG_PAYMENT_15', 'Your Contact Information');
define('APP_LANG_PAYMENT_16', 'Name');
define('APP_LANG_PAYMENT_17', 'Name of buyer/payer. Only alphabet and spaces. Special characters such as & are not allowed');
define('APP_LANG_PAYMENT_18', 'Email');
define('APP_LANG_PAYMENT_19', 'Email of buyer/payer. We will send payment confirmation to this email');
define('APP_LANG_PAYMENT_20', 'Mobile Number');
define('APP_LANG_PAYMENT_21', 'Mobile number such as 0128888888');
define('APP_LANG_PAYMENT_22', 'Delivery Details');
define('APP_LANG_PAYMENT_23', 'Delivery Type');
define('APP_LANG_PAYMENT_24', 'Deliver to my address');
define('APP_LANG_PAYMENT_25', 'Self pickup');
define('APP_LANG_PAYMENT_26', 'Address 1');
define('APP_LANG_PAYMENT_27', 'Address 1');
define('APP_LANG_PAYMENT_28', 'Address 2');
define('APP_LANG_PAYMENT_29', 'Address 2');
define('APP_LANG_PAYMENT_30', 'City');
define('APP_LANG_PAYMENT_31', 'City');
define('APP_LANG_PAYMENT_32', 'Postal Code');
define('APP_LANG_PAYMENT_33', 'Postal Code');
define('APP_LANG_PAYMENT_34', 'State');
define('APP_LANG_PAYMENT_35', 'Country');
define('APP_LANG_PAYMENT_36', 'Payment Detail');
define('APP_LANG_PAYMENT_37', 'Credit / Debit Card');
define('APP_LANG_PAYMENT_38', 'Internet Banking');
define('APP_LANG_PAYMENT_39', 'Name');
define('APP_LANG_PAYMENT_41', 'Name as displayed on card');
define('APP_LANG_PAYMENT_42', 'Credit / Debit Card Number');
define('APP_LANG_PAYMENT_43', 'Credit / debit card number as displayed on card');
define('APP_LANG_PAYMENT_44', 'Expiry Date');
define('APP_LANG_PAYMENT_45', 'Security Code');
define('APP_LANG_PAYMENT_46', 'Security code or CVV');
define('APP_LANG_PAYMENT_47', 'You can also use your ATM card as debit card. <a href="https://guide.senangpay.my/payment-process-with-debit-card/" target="_blank">Click here</a>');
define('APP_LANG_PAYMENT_48', 'Yes, I agree and I have read all the <a href="' . APP_TERMS_AND_CONDITIONS_URL . '" target="_blank">Terms & Conditions</a>');
define('APP_LANG_PAYMENT_49', 'Yes, I authorised ' . APP . ' (' . APP_COMPANY_NAME . ') to debit the above net charges in my credit card or account');
define('APP_LANG_PAYMENT_50', 'If you suspect that this is a fraud. Please <a href="' . APP_FRAUD_SUSPECT_URL . '" target="_blank">here</a> to make a report');
define('APP_LANG_PAYMENT_51', 'Please disable any pop-up blocker');
define('APP_LANG_PAYMENT_52', 'Submit');
define('APP_LANG_PAYMENT_53', 'Size');
define('APP_LANG_PAYMENT_54', 'Product Name');
define('APP_LANG_PAYMENT_55', 'Product Name');
define('APP_LANG_PAYMENT_56', 'Total Price');
define('APP_LANG_PAYMENT_57', 'Product Price for a unit, example 10.20');
define('APP_LANG_PAYMENT_58', 'Min amount allowed is RM 2');
define('APP_LANG_PAYMENT_59', 'Recurring Payment Info');
define('APP_LANG_PAYMENT_60', 'By making this payment you are agreeing that we will charge your credit / debit card for the payment scheduled as below. The first payment in the table below refers to the current payment.');
define('APP_LANG_PAYMENT_61', 'Payment schedule');
define('APP_LANG_PAYMENT_62', 'No');
define('APP_LANG_PAYMENT_63', 'Payment Date');
define('APP_LANG_PAYMENT_64', 'Amount (RM)');
define('APP_LANG_PAYMENT_65', 'Today');
define('APP_LANG_PAYMENT_66', 'Grand Total (RM)');
define('APP_LANG_PAYMENT_67', 'Jan');
define('APP_LANG_PAYMENT_68', 'Feb');
define('APP_LANG_PAYMENT_69', 'Mar');
define('APP_LANG_PAYMENT_70', 'Apr');
define('APP_LANG_PAYMENT_71', 'May');
define('APP_LANG_PAYMENT_72', 'Jun');
define('APP_LANG_PAYMENT_73', 'Jul');
define('APP_LANG_PAYMENT_74', 'Aug');
define('APP_LANG_PAYMENT_75', 'Sep');
define('APP_LANG_PAYMENT_76', 'Oct');
define('APP_LANG_PAYMENT_77', 'Nov');
define('APP_LANG_PAYMENT_78', 'Dec');
define('APP_LANG_PAYMENT_79', 'Bank');
define('APP_LANG_PAYMENT_80', '<a href="https://guide.senangpay.my/fpx-availability-time/" target="_blank">Click here</a> to find out why I did not see some of the banks in the list');
define('APP_LANG_PAYMENT_81', 'Due to FPX issue, if you choose CIMB bank you will require to choose the bank again at FPX website');
define('APP_LANG_PAYMENT_82', '* There is no GST charge for this product');
define('APP_LANG_PAYMENT_83', 'Payment Failed');
define('APP_LANG_PAYMENT_84', 'Sorry, we unable to process your payment. It seems that there is a problem with your credit card / bank account / boost account / GrabPay account. Please check with your bank for more information.');
define('APP_LANG_PAYMENT_85', 'Try again');
define('APP_LANG_PAYMENT_86', 'Back to ');
define('APP_LANG_PAYMENT_87', 'Discount Amount (RM) ');
define('APP_LANG_PAYMENT_88', 'Required Additional Field');
define('APP_LANG_PAYMENT_89', 'Mandatory field');
define('APP_LANG_PAYMENT_90', 'Discount Code');
define('APP_LANG_PAYMENT_91', 'Enter your discount code if you have any');
define('APP_LANG_PAYMENT_92', 'Product info');
define('APP_LANG_PAYMENT_93', 'View Product Info');
define('APP_LANG_PAYMENT_94', 'Confirm Order');
define('APP_LANG_PAYMENT_95', 'Recurring product quantity is set to only one (1) and cannot be changed');
define('APP_LANG_PAYMENT_96', 'Select your size');
define('APP_LANG_PAYMENT_97', 'Select delivery type');
define('APP_LANG_PAYMENT_98', 'Order Summary');
define('APP_LANG_PAYMENT_99', 'Discounts (if any)');
define('APP_LANG_PAYMENT_100', 'Choose Payment Method');
define('APP_LANG_PAYMENT_101', 'Card Holder Name');
define('APP_LANG_PAYMENT_102', 'Month');
define('APP_LANG_PAYMENT_103', 'Year');
define('APP_LANG_PAYMENT_104', 'CVV');
define('APP_LANG_PAYMENT_105', 'Additional Information');
define('APP_LANG_PAYMENT_106', 'Error Adding Item');
define('APP_LANG_PAYMENT_107', 'Quantity cannot be empty');
define('APP_LANG_PAYMENT_108', 'Quantity cannot be less than 1');
define('APP_LANG_PAYMENT_109', 'Quantity cannot be more than 10');
define('APP_LANG_PAYMENT_110', 'Please choose the item size');
define('APP_LANG_PAYMENT_111', 'Please choose or fill in the "__field__" field');
define('APP_LANG_PAYMENT_112', 'P/S: Don\'t worry, there is no deduction occured yet.');
define('APP_LANG_PAYMENT_113', 'Select And Add A Product First');
define('APP_LANG_PAYMENT_114', 'Please fill in the product name');
define('APP_LANG_PAYMENT_115', 'Please fill in the price');
define('APP_LANG_PAYMENT_116', 'Additional Input');
define('APP_LANG_PAYMENT_117', 'Select Item');
define('APP_LANG_PAYMENT_118', 'Make Payment ( Credit / Debit Card )');
define('APP_LANG_PAYMENT_119', 'Make Payment ( Internet Banking FPX )');
define('APP_LANG_PAYMENT_120', 'Sign Up');
define('APP_LANG_PAYMENT_121', 'T&amp;C');
define('APP_LANG_PAYMENT_122', 'Report Fraud');
define('APP_LANG_PAYMENT_123', 'By proceeding, you agree to authorise senangPay (Simplepay Gateway Sdn Bhd) to debit the above net charges to your credit/debit card or online banking account.');
define('APP_LANG_PAYMENT_124', 'Pay');
define('APP_LANG_PAYMENT_125', 'No item selected');
define('APP_LANG_PAYMENT_126', 'Click on item to remove');
define('APP_LANG_PAYMENT_127', 'Item Selected');
define('APP_LANG_PAYMENT_128', 'Item have been selected.');
define('APP_LANG_PAYMENT_129', 'Delivery Charge Updated');
define('APP_LANG_PAYMENT_130', 'Delivery charge have been updated.');
define('APP_LANG_PAYMENT_131', 'Please select an item first.');
define('APP_LANG_PAYMENT_132', 'Incomplete Item Details');
define('APP_LANG_PAYMENT_133', 'Recurring Price');
define('APP_LANG_PAYMENT_134', 'Recurring Schedule');
define('APP_LANG_PAYMENT_135', 'Date');
define('APP_LANG_PAYMENT_136', 'Amount');
define('APP_LANG_PAYMENT_137', 'Balance');
define('APP_LANG_PAYMENT_138', 'Prorate');
define('APP_LANG_PAYMENT_139', 'Your credit / debit card will be charged every month on the %s.');
define('APP_LANG_PAYMENT_140', 'Status');
define('APP_LANG_PAYMENT_141', 'Your credit / debit card will be charged on %s every year.');
define('APP_LANG_PAYMENT_142', 'Your credit / debit card will be charged on %s, %s, %s and %s.');
define('APP_LANG_PAYMENT_143', 'Your credit / debit card will be charged on %s and %s.');
define('APP_LANG_PAYMENT_144', 'Your credit / debit card will be charged every 2 months on the %s.');
define('APP_LANG_PAYMENT_145', 'Your credit / debit card will be charged every 4 months on the %s.');
define('APP_LANG_PAYMENT_146', 'Your credit / debit card will be charged every 5 months on the %s.');
define('APP_LANG_PAYMENT_147', 'Your credit / debit card will be charged every 7 months on the %s.');
define('APP_LANG_PAYMENT_148', 'Your credit / debit card will be charged every 8 months on the %s.');
define('APP_LANG_PAYMENT_149', 'Your credit / debit card will be charged every 9 months on the %s.');
define('APP_LANG_PAYMENT_150', 'Your credit / debit card will be charged every 10 months on the %s.');
define('APP_LANG_PAYMENT_151', 'Your credit / debit card will be charged every 11 months on the %s.');
define('APP_LANG_PAYMENT_152', 'SST Rate');
define('APP_LANG_PAYMENT_153', 'SST');
define('APP_LANG_PAYMENT_154', 'This is to check your credit/debit card is chargable');
define('APP_LANG_PAYMENT_155', 'Credit / Debit Card');
define('APP_LANG_PAYMENT_156', 'Card Validation');
define('APP_LANG_PAYMENT_157', 'By submitting this form you agree that your card detail will be stored and will be use for future transaction. Take note that you will not be charge for this validation process.');
define('APP_LANG_PAYMENT_158', 'Payment Summary');
define('APP_LANG_PAYMENT_159', 'Payment Details');
define('APP_LANG_PAYMENT_160', 'By submitting this form you agree that your card will be charged RM %s for %m.');
define('APP_LANG_PAYMENT_161', 'Make Payment ( Boost )');
define('APP_LANG_PAYMENT_162', 'Boost');
define('APP_LANG_PAYMENT_163', 'CC');
define('APP_LANG_PAYMENT_164', 'FPX');
define('APP_LANG_PAYMENT_165', 'Touch n GO');
define('APP_LANG_PAYMENT_166', 'TnG');
define('APP_LANG_PAYMENT_167', 'Grand Total ');
define('APP_STRIPE_TEST_PUBLISHABLE_KEY', 'pk_test_51H04R3Dh7WJaKwJAOlv9HeJswm3Vpm66fcFHdRH7sanj56d3mXu6KXgGuLYHbfmQqQftsQWX35ySkul528Z4CPlJ00s6ZS8b1Q');

$data = array(
    'business_registration_name' => 'Bobo',
    'header_name' => 'Bobo',
    'business_registration' => 'Bobo',
    'business_registration_no' => '123',
    'header_email' => 'dd@fff.com',
    'header_phone' => '2323233',
    'sst_no' => '123',
    'header_instagram' => '123',
    'header_blog' => '123',
    'header_facebook' => '123',
    'currency' => 'MYR',
    'amount' => '10',
    'detail' => 'Ayam Masak Merah',
    'transaction_reference' => '123',
    'name' => 'Abu',
    'publishable_key' => 'Abu',
);

$data['stripe_other_payment_methods'] = array();

#include ewallet or fpx payment availability if this is MYR currency
$data['stripe_other_payment_methods'][] = 'boost';

$data['stripe_other_payment_methods'][] = 'tng';

$data['stripe_other_payment_methods'][] = 'grabpay';

$data['stripe_other_payment_methods'][] = 'fpx';
