<?php

/**
 * Controller for api
 * 
 * @author Syed Zulazri <zulazri@secretlab.media>
 * @copyright (c) 2015, Secretlab.Media Sdn Bhd (Project: simplepay)
 */
class reporting_controller extends slm_controller
{

    function __construct()
    {
        parent::__construct();
    }

    function all_action()
    {
        $signup = $this->signup_current();
//        $suspended = $this->suspended_current();
        $txn = $this->txn_current();

        $all_data = array(
            'signup' => $signup,
//            'suspended' => $suspended,
            'txn' => $txn,
        );

        $this->post_data(json_encode($all_data));
    }

    function signup_current()
    {
        $db = new db();

        $start_timestamp = strtotime('-2 minutes');
        $end_timestamp = strtotime('now');

        $sql = '';
        $placeholder = array();
        $counter = 1;

        if ($start_timestamp >= 0 && $end_timestamp != 0 && $end_timestamp > $start_timestamp)
        {

            if ($sql == '')
                $sql .= ' WHERE tm.date_created >= ? AND tm.date_created <= ?';
            else
                $sql .= ' AND tm.date_created >= ? AND tm,date_created <= ?';

            $placeholder[$counter] = array('value' => $start_timestamp, 'type' => 'int');
            $counter++;
            $placeholder[$counter] = array('value' => $end_timestamp, 'type' => 'int');
            $counter++;
        }

        $paidstatus = status::get_status_given_name('paid');
        $sql .= ' AND tm.status_id = ?';
        $placeholder[$counter] = array('value' => $paidstatus->id, 'type' => 'int');
        $counter++;

        $limit_stmt = '';
        $result = $db->query_new('SELECT tm.amount,m.package_id,m.name as merchant_name,m.id as merchant_id,transaction_reference,tm.id,tm.type,tm.status_id,payment_type,tm.date_created'
                . ' ,(SELECT COUNT(*) FROM merchant_affiliate ma WHERE ma.merchant_id = tm.merchant_id) as is_from_affiliate FROM transaction_merchant tm'
                . ' LEFT JOIN merchant m ON m.id = tm.merchant_id  ' . $sql . ' ORDER BY tm.id DESC' . $limit_stmt, $placeholder);

        return ($result);
    }

    function txn_current()
    {
        $db = new db();

        $start_timestamp = strtotime('-2 minutes');

        $end_timestamp = strtotime('now');

        $sql = '';
        $placeholder = array();
        $counter = 1;

        if ($start_timestamp >= 0 && $end_timestamp != 0 && $end_timestamp > $start_timestamp)
        {

            if ($sql == '')
                $sql .= ' WHERE tm.date_created >= ? AND tm.date_created <= ?';
            else
                $sql .= ' AND tm.date_created >= ? AND tm,date_created <= ?';

            $placeholder[$counter] = array('value' => $start_timestamp, 'type' => 'int');
            $counter++;
            $placeholder[$counter] = array('value' => $end_timestamp, 'type' => 'int');
            $counter++;
        }

        $paidstatus = status::get_status_given_name('paid');
        $sql .= ' AND tm.status_id = ?';
        $placeholder[$counter] = array('value' => $paidstatus->id, 'type' => 'int');
        $counter++;

        $limit_stmt = '';
        $result = $db->query_new('SELECT tm.customer_name,tm.email,tm.price_total,tm.price_total,m.name as merchant_name,m.id as merchant_id,transaction_reference,tm.id,tm.status_id,payment_type,tm.date_created'
                . ' FROM transaction tm'
                . ' LEFT JOIN merchant m ON m.id = tm.merchant_id  ' . $sql . ' ORDER BY tm.id DESC' . $limit_stmt, $placeholder);

        return ($result);
    }

    function post_data($rawdata)
    {
//print_r($rawdata);die;
        $url = 'http://report.senangpay.my/api/report';
        $ch = curl_init($url);
        $data_string = ($rawdata);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array("data" => $data_string));

        $result = curl_exec($ch);
        curl_close($ch);
    }

}

?>
