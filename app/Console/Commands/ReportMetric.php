<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class ReportMetric extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:metric';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->signupdaily();
        $this->signupmonhtly(date('m'), date('Y'));
//        $this->signupmonhtly(12, 2018);
        $this->signupyearly();
        $this->renewdaily();
        $this->renewmonhtly(date('m'), date('Y'));
        $this->renewmonhtly('02', '2019');
        $this->renewyearly();
        $this->affiliatedaily();
        $this->affiliatemonthly(date('m'), date('Y'));
        $this->affiliatemonthly('02', '2019');
        $this->affiliateyearly();
        $this->txndaily();
        $this->txnmonthly(date('m'), date('Y'));
//        $this->txnmonthly(12, 2018);
        $this->txnyearly();
    }

    private function get_package_array_given_tier($tier)
    {
        $packages = \App\Models\Package::where('tier', $tier)->where('is_public', 1)->where('type', 'yearly')->get();

        return $packages->pluck('id');
    }

    private function signupdaily()
    {
        $signupDailyTotal = \App\Models\Signup::where('type', 'registration')->whereDate('created_at', '=', date('Y-m-d'))->count();
        $signupDailyAmount = \App\Models\Signup::where('type', 'registration')->whereDate('created_at', '=', date('Y-m-d'))->sum('value');
        $signupDailyAmount = str_replace(",", "", $signupDailyAmount);

        $dbSignupDaily = \App\Models\SignupDaily::whereDate('date', '=', date('Y-m-d'))->first();
        if (!$dbSignupDaily)
        {
            $dbSignupDaily = new \App\Models\SignupDaily;
        }

        $prevDate = date('Y-m-d', strtotime("-1 days"));
        $prev = \App\Models\SignupDaily::whereDate('date', '=', $prevDate)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupDaily->total_status = $prevTotal > $signupDailyTotal ? 'down' : 'up';
        $dbSignupDaily->total_amount_status = $prevTotalAmount > $signupDailyAmount ? 'down' : 'up';
        $dbSignupDaily->total_status_percent = number_format(($signupDailyTotal - $prevTotal) / $this->checkamount($signupDailyTotal) * 100, 0);
        $dbSignupDaily->total_amount_status_percent = number_format(($signupDailyAmount - $prevTotalAmount) / $this->checkamount($signupDailyAmount) * 100, 0);

        $dbSignupDaily->total = $signupDailyTotal;
        $dbSignupDaily->date = date('Y-m-d');
        $dbSignupDaily->total_amount = number_format($signupDailyAmount, 2);
        $dbSignupDaily->save();
    }

    private function signupmonhtly($m, $y)
    {
        $signupMonthlyFPX = \App\Models\Signup::where('type', 'registration')->where('payment_type', '=', 'FPX')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonthlyCC = \App\Models\Signup::where('type', 'registration')->where('payment_type', '=', 'Credit Card')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();

        $signupMonthlyAmountFPX = \App\Models\Signup::where('type', 'registration')->where('payment_type', '=', 'FPX')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountFPX = str_replace(",", "", $signupMonthlyAmountFPX);

        $signupMonthlyAmountCC = \App\Models\Signup::where('type', 'registration')->where('payment_type', '=', 'Credit Card')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountCC = str_replace(",", "", $signupMonthlyAmountCC);

        $signupMonhtlyTotal = \App\Models\Signup::whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->where('type', '!=', 'other')->count();
        $signupMonhtlyTotalAffiliate = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalNonAffiliate = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 0)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();

        $basic_packages = $this->get_package_array_given_tier(1);
        $advanced_packages = $this->get_package_array_given_tier(2);
        $special_packages = $this->get_package_array_given_tier(3);

        $signupMonhtlyTotalBasic = \App\Models\Signup::whereIn('package_id', $basic_packages)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalAdvanced = \App\Models\Signup::whereIn('package_id', $advanced_packages)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalSpecial = \App\Models\Signup::whereIn('package_id', $special_packages)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyAffiliateBasic = \App\Models\Signup::whereIn('package_id', $basic_packages)->where('type', 'registration')->where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();

        $signupMonhtlyAffiliateAdvanced = \App\Models\Signup::whereIn('package_id', $advanced_packages)->where('is_from_affiliate', 1)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyAffiliateSpecial = \App\Models\Signup::whereIn('package_id', $special_packages)->where('is_from_affiliate', 1)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonthlyAmount = \App\Models\Signup::where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmount = str_replace(",", "", $signupMonthlyAmount);

        $signupMonthlyAmountAffiliate = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountAffiliate = str_replace(",", "", $signupMonthlyAmountAffiliate);

        $signupMonthlyAmountNonAffiliate = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 0)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountNonAffiliate = str_replace(",", "", $signupMonthlyAmountNonAffiliate);

        $signupMonthlyAmountBasic = \App\Models\Signup::whereIn('package_id', $basic_packages)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountBasic = str_replace(",", "", $signupMonthlyAmountBasic);
        $signupMonthlyAmountAdvanced = \App\Models\Signup::whereIn('package_id', $advanced_packages)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountAdvanced = str_replace(",", "", $signupMonthlyAmountAdvanced);
        $signupMonthlyAmountSpecial = \App\Models\Signup::whereIn('package_id', $special_packages)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountSpecial = str_replace(",", "", $signupMonthlyAmountSpecial);

        $signupMonthlyAmountAffiliateBasic = \App\Models\Signup::whereIn('package_id', $basic_packages)->where('is_from_affiliate', 1)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountAffiliateBasic = str_replace(",", "", $signupMonthlyAmountAffiliateBasic);

        $signupMonthlyAmountAffiliateAdvanced = \App\Models\Signup::whereIn('package_id', $advanced_packages)->where('is_from_affiliate', 1)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountAffiliateAdvanced = str_replace(",", "", $signupMonthlyAmountAffiliateBasic);

        $signupMonthlyAmountAffiliateSpecial = \App\Models\Signup::whereIn('package_id', $special_packages)->where('is_from_affiliate', 1)->where('type', 'registration')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmountAffiliateSpecial = str_replace(",", "", $signupMonthlyAmountAffiliateSpecial);

        $dbSignupMonthly = \App\Models\SignupMonthly::where('month', '=', $m)->where('year', '=', $y)->first();
        if (!$dbSignupMonthly)
        {
            $dbSignupMonthly = new \App\Models\SignupMonthly;
        }
        $prevMonth = date('m', strtotime('first day of previous month'));
        $year = date('Y', strtotime('first day of previous month'));
        $prev = \App\Models\SignupMonthly::where('month', '=', $prevMonth)->where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupMonthly->total = $signupMonhtlyTotal;
        $dbSignupMonthly->total_status = $prevTotal > $signupMonhtlyTotal ? 'down' : 'up';
        $dbSignupMonthly->total_amount_status = $prevTotalAmount > $signupMonthlyAmount ? 'down' : 'up';
        $dbSignupMonthly->total_status_percent = number_format(($signupMonhtlyTotal - $prevTotal) / $this->checkamount($signupMonhtlyTotal) * 100, 0);
        $dbSignupMonthly->total_amount_status_percent = number_format(($signupMonthlyAmount - $prevTotalAmount) / $this->checkamount($signupMonthlyAmount) * 100, 0);

        $dbSignupMonthly->total_fpx = $signupMonthlyFPX;
        $dbSignupMonthly->total_cc = $signupMonthlyCC;
        $dbSignupMonthly->total_amount_fpx = $signupMonthlyAmountFPX;
        $dbSignupMonthly->total_amount_cc = $signupMonthlyAmountCC;

        $dbSignupMonthly->total_affiliate = $signupMonhtlyTotalAffiliate;
        $dbSignupMonthly->total_nonaffiliate = $signupMonhtlyTotalNonAffiliate;
        $dbSignupMonthly->total_basic = $signupMonhtlyTotalBasic;
        $dbSignupMonthly->total_advanced = $signupMonhtlyTotalAdvanced;
        $dbSignupMonthly->total_special = $signupMonhtlyTotalSpecial;
        $dbSignupMonthly->total_affiliate_basic = $signupMonhtlyAffiliateBasic;
        $dbSignupMonthly->total_affiliate_advanced = $signupMonhtlyAffiliateAdvanced;
        $dbSignupMonthly->total_affiliate_special = $signupMonhtlyAffiliateSpecial;
        $dbSignupMonthly->total_nonaffiliate_basic = $signupMonhtlyTotalBasic - $signupMonhtlyAffiliateBasic;
        $dbSignupMonthly->total_nonaffiliate_advanced = $signupMonhtlyTotalAdvanced - $signupMonhtlyAffiliateAdvanced;
        $dbSignupMonthly->total_nonaffiliate_special = $signupMonhtlyTotalSpecial - $signupMonhtlyTotalSpecial;
        $dbSignupMonthly->month = $m;
        $dbSignupMonthly->year = $y;
        $dbSignupMonthly->total_amount = number_format($signupMonthlyAmount, 2);
        $dbSignupMonthly->total_amount_affiliate = number_format($signupMonthlyAmountAffiliate, 2);
        $dbSignupMonthly->total_amount_nonaffiliate = number_format($signupMonthlyAmountNonAffiliate, 2);
        $dbSignupMonthly->total_amount_basic = number_format($signupMonthlyAmountBasic, 2);
        $dbSignupMonthly->total_amount_advanced = number_format($signupMonthlyAmountAdvanced, 2);
        $dbSignupMonthly->total_amount_special = number_format($signupMonthlyAmountSpecial, 2);
        $dbSignupMonthly->total_amount_affiliate_basic = number_format($signupMonthlyAmountAffiliateBasic, 2);
        $dbSignupMonthly->total_amount_affiliate_advanced = number_format($signupMonthlyAmountAffiliateAdvanced, 2);
        $dbSignupMonthly->total_amount_affiliate_special = number_format($signupMonthlyAmountAffiliateSpecial, 2);
        $dbSignupMonthly->total_amount_nonaffiliate_basic = number_format($signupMonthlyAmountBasic - $signupMonthlyAmountAffiliateBasic, 2);
        $dbSignupMonthly->total_amount_nonaffiliate_advanced = number_format($signupMonthlyAmountAdvanced - $signupMonthlyAmountAffiliateAdvanced, 2);
        $dbSignupMonthly->total_amount_nonaffiliate_special = number_format($signupMonthlyAmountSpecial - $signupMonthlyAmountAffiliateSpecial, 2);
        $dbSignupMonthly->save();
    }

    private function signupyearly()
    {
        $signupYearlyTotal = \App\Models\Signup::where('type', 'registration')->whereYear('created_at', '=', date('Y'))->count();
        $signupYearlyAmount = \App\Models\Signup::where('type', 'registration')->whereYear('created_at', '=', date('Y'))->sum('value');
        $signupYearlyAmount = str_replace(",", "", $signupYearlyAmount);

        $dbSignupYearly = \App\Models\SignupYearly::where('year', '=', date('Y'))->first();
        if (!$dbSignupYearly)
        {
            $dbSignupYearly = new \App\Models\SignupYearly;
        }
        $year = date('Y', strtotime('first day of previous year'));
        $prev = \App\Models\SignupYearly::where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupYearly->total_status = $prevTotal > $signupYearlyTotal ? 'down' : 'up';
        $dbSignupYearly->total_amount_status = $prevTotalAmount > $signupYearlyAmount ? 'down' : 'up';
        $dbSignupYearly->total_status_percent = number_format(($signupYearlyTotal - $prevTotal) / $this->checkamount($signupYearlyTotal) * 100, 0);
        $dbSignupYearly->total_amount_status_percent = number_format(($signupYearlyAmount - $prevTotalAmount) / $this->checkamount($signupYearlyAmount) * 100, 0);

        $dbSignupYearly->total = $signupYearlyTotal;
        $dbSignupYearly->year = date('Y');
        $dbSignupYearly->total_amount = number_format($signupYearlyAmount, 2);
        $dbSignupYearly->save();
    }

    private function affiliatedaily()
    {
        $signupDailyTotal = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereDate('created_at', '=', date('Y-m-d'))->count();
        $signupDailyAmount = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereDate('created_at', '=', date('Y-m-d'))->sum('value');
        $signupDailyAmount = str_replace(",", "", $signupDailyAmount);

        $dbSignupDaily = \App\Models\AffiliateDaily::whereDate('date', '=', date('Y-m-d'))->first();
        if (!$dbSignupDaily)
        {
            $dbSignupDaily = new \App\Models\AffiliateDaily;
        }

        $prevDate = date('Y-m-d', strtotime("-1 days"));
        $prev = \App\Models\AffiliateDaily::whereDate('date', '=', $prevDate)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupDaily->total_status = $prevTotal > $signupDailyTotal ? 'down' : 'up';
        $dbSignupDaily->total_amount_status = $prevTotalAmount > $signupDailyAmount ? 'down' : 'up';
        $dbSignupDaily->total_status_percent = number_format(($signupDailyTotal - $prevTotal) / $this->checkamount($signupDailyTotal) * 100, 0);
        $dbSignupDaily->total_amount_status_percent = number_format(($signupDailyAmount - $prevTotalAmount) / $this->checkamount($signupDailyAmount) * 100, 0);

        $dbSignupDaily->total = $signupDailyTotal;
        $dbSignupDaily->date = date('Y-m-d');
        $dbSignupDaily->total_amount = number_format($signupDailyAmount, 2);
        $dbSignupDaily->save();
    }

    private function affiliatemonthly($m, $y)
    {
        $signupMonhtlyTotal = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonthlyAmount = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmount = str_replace(",", "", $signupMonthlyAmount);

        $dbSignupMonthly = \App\Models\AffiliateMonthly::where('month', '=', $m)->where('year', '=', $y)->first();
        if (!$dbSignupMonthly)
        {
            $dbSignupMonthly = new \App\Models\AffiliateMonthly;
        }
        $prevMonth = date('m', strtotime('first day of previous month'));
        $year = date('Y', strtotime('first day of previous month'));
        $prev = \App\Models\AffiliateMonthly::where('month', '=', $prevMonth)->where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupMonthly->total_status = $prevTotal > $signupMonhtlyTotal ? 'down' : 'up';
        $dbSignupMonthly->total_amount_status = $prevTotalAmount > $signupMonthlyAmount ? 'down' : 'up';
        $dbSignupMonthly->total_status_percent = number_format(($signupMonhtlyTotal - $prevTotal) / $this->checkamount($signupMonhtlyTotal) * 100, 0);
        $dbSignupMonthly->total_amount_status_percent = number_format(($signupMonthlyAmount - $prevTotalAmount) / $this->checkamount($signupMonthlyAmount) * 100, 0);

        $dbSignupMonthly->total = $signupMonhtlyTotal;
        $dbSignupMonthly->month = $m;
        $dbSignupMonthly->year = $y;
        $dbSignupMonthly->total_amount = number_format($signupMonthlyAmount, 2);
        $dbSignupMonthly->save();
    }

    private function affiliateyearly()
    {
        $signupYearlyTotal = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereYear('created_at', '=', date('Y'))->count();
        $signupYearlyAmount = \App\Models\Signup::where('type', 'registration')->where('is_from_affiliate', 1)->whereYear('created_at', '=', date('Y'))->sum('value');
        $signupYearlyAmount = str_replace(",", "", $signupYearlyAmount);

        $dbSignupYearly = \App\Models\AffiliateYearly::where('year', '=', date('Y'))->first();
        if (!$dbSignupYearly)
        {
            $dbSignupYearly = new \App\Models\AffiliateYearly;
        }
        $year = date('Y', strtotime('first day of previous year'));
        $prev = \App\Models\AffiliateYearly::where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupYearly->total_status = $prevTotal > $signupYearlyTotal ? 'down' : 'up';
        $dbSignupYearly->total_amount_status = $prevTotalAmount > $signupYearlyAmount ? 'down' : 'up';
        $dbSignupYearly->total_status_percent = number_format(($signupYearlyTotal - $prevTotal) / $this->checkamount($signupYearlyTotal) * 100, 0);
        $dbSignupYearly->total_amount_status_percent = number_format(($signupYearlyAmount - $prevTotalAmount) / $this->checkamount($signupYearlyAmount) * 100, 0);

        $dbSignupYearly->total = $signupYearlyTotal;
        $dbSignupYearly->year = date('Y');
        $dbSignupYearly->total_amount = number_format($signupYearlyAmount, 2);
        $dbSignupYearly->save();
    }

    private function renewdaily()
    {
        $signupDailyTotal = \App\Models\Signup::where('type', 'renewal')->whereDate('created_at', '=', date('Y-m-d'))->count();
        $signupDailyAmount = \App\Models\Signup::where('type', 'renewal')->whereDate('created_at', '=', date('Y-m-d'))->sum('value');
        $signupDailyAmount = str_replace(",", "", $signupDailyAmount);

        $dbSignupDaily = \App\Models\RenewDaily::whereDate('date', '=', date('Y-m-d'))->first();
        if (!$dbSignupDaily)
        {
            $dbSignupDaily = new \App\Models\RenewDaily;
        }

        $prevDate = date('Y-m-d', strtotime("-1 days"));
        $prev = \App\Models\RenewDaily::whereDate('date', '=', $prevDate)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupDaily->total_status = $prevTotal > $signupDailyTotal ? 'down' : 'up';
        $dbSignupDaily->total_amount_status = $prevTotalAmount > $signupDailyAmount ? 'down' : 'up';
        $dbSignupDaily->total_status_percent = number_format(($signupDailyTotal - $prevTotal) / $this->checkamount($signupDailyTotal) * 100, 0);
        $dbSignupDaily->total_amount_status_percent = number_format(($signupDailyAmount - $prevTotalAmount) / $this->checkamount($signupDailyAmount) * 100, 0);

        $dbSignupDaily->total = $signupDailyTotal;
        $dbSignupDaily->date = date('Y-m-d');
        $dbSignupDaily->total_amount = number_format($signupDailyAmount, 2);
        $dbSignupDaily->save();
    }

    private function renewmonhtly($m, $y)
    {
        $signupMonhtlyTotal = \App\Models\Signup::where('type', 'renewal')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonthlyAmount = \App\Models\Signup::where('type', 'renewal')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('value');
        $signupMonthlyAmount = str_replace(",", "", $signupMonthlyAmount);

        $dbSignupMonthly = \App\Models\RenewMonthly::where('month', '=', $m)->where('year', '=', $y)->first();
        if (!$dbSignupMonthly)
        {
            $dbSignupMonthly = new \App\Models\RenewMonthly;
        }
        $prevMonth = date('m', strtotime('first day of previous month'));
        $year = date('Y', strtotime('first day of previous month'));
        $prev = \App\Models\RenewMonthly::where('month', '=', $prevMonth)->where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupMonthly->total_status = $prevTotal > $signupMonhtlyTotal ? 'down' : 'up';
        $dbSignupMonthly->total_amount_status = $prevTotalAmount > $signupMonthlyAmount ? 'down' : 'up';
        $dbSignupMonthly->total_status_percent = number_format(($signupMonhtlyTotal - $prevTotal) / $this->checkamount($signupMonhtlyTotal) * 100, 0);
        $dbSignupMonthly->total_amount_status_percent = number_format(($signupMonthlyAmount - $prevTotalAmount) / $this->checkamount($signupMonthlyAmount) * 100, 0);

        $dbSignupMonthly->total = $signupMonhtlyTotal;
        $dbSignupMonthly->month = $m;
        $dbSignupMonthly->year = $y;
        $dbSignupMonthly->total_amount = number_format($signupMonthlyAmount, 2);
        $dbSignupMonthly->save();
    }

    private function renewyearly()
    {
        $signupYearlyTotal = \App\Models\Signup::where('type', 'renewal')->whereYear('created_at', '=', date('Y'))->count();
        $signupYearlyAmount = \App\Models\Signup::where('type', 'renewal')->whereYear('created_at', '=', date('Y'))->sum('value');
        $signupYearlyAmount = str_replace(",", "", $signupYearlyAmount);

        $dbSignupYearly = \App\Models\RenewYearly::where('year', '=', date('Y'))->first();
        if (!$dbSignupYearly)
        {
            $dbSignupYearly = new \App\Models\RenewYearly;
        }
        $year = date('Y', strtotime('first day of previous year'));
        $prev = \App\Models\RenewYearly::where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupYearly->total_status = $prevTotal > $signupYearlyTotal ? 'down' : 'up';
        $dbSignupYearly->total_amount_status = $prevTotalAmount > $signupYearlyAmount ? 'down' : 'up';
        $dbSignupYearly->total_status_percent = number_format(($signupYearlyTotal - $prevTotal) / $this->checkamount($signupYearlyTotal) * 100, 0);
        $dbSignupYearly->total_amount_status_percent = number_format(($signupYearlyAmount - $prevTotalAmount) / $this->checkamount($signupYearlyAmount) * 100, 0);

        $dbSignupYearly->total = $signupYearlyTotal;
        $dbSignupYearly->year = date('Y');
        $dbSignupYearly->total_amount = number_format($signupYearlyAmount, 2);
        $dbSignupYearly->save();
    }

    private function txndaily()
    {
        $signupDailyTotal = \App\Models\Txn::whereDate('created_at', '=', date('Y-m-d'))->count();
        $signupDailyAmount = \App\Models\Txn::whereDate('created_at', '=', date('Y-m-d'))->sum('price_total');
        $signupDailyAmount = str_replace(",", "", $signupDailyAmount);

        $dbSignupDaily = \App\Models\TxnDaily::whereDate('date', '=', date('Y-m-d'))->first();
        if (!$dbSignupDaily)
        {
            $dbSignupDaily = new \App\Models\TxnDaily;
        }

        $prevDate = date('Y-m-d', strtotime("-1 days"));
        $prev = \App\Models\TxnDaily::whereDate('date', '=', $prevDate)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupDaily->total_status = $prevTotal > $signupDailyTotal ? 'down' : 'up';
        $dbSignupDaily->total_amount_status = $prevTotalAmount > $signupDailyAmount ? 'down' : 'up';
        $dbSignupDaily->total_status_percent = number_format(($signupDailyTotal - $prevTotal) / $this->checkamount($signupDailyTotal) * 100, 0);
        $dbSignupDaily->total_amount_status_percent = number_format(($signupDailyAmount - $prevTotalAmount) / $this->checkamount($signupDailyAmount) * 100, 0);

        $dbSignupDaily->total = $signupDailyTotal;
        $dbSignupDaily->date = date('Y-m-d');
        $dbSignupDaily->total_amount = number_format($signupDailyAmount, 2);
        $dbSignupDaily->save();
    }

    private function txnmonthly($m, $y)
    {
        $basic_packages = $this->get_package_array_given_tier(1);
        $advanced_packages = $this->get_package_array_given_tier(2);
        $special_packages = $this->get_package_array_given_tier(3);

        $signupMonhtlyTotal = \App\Models\Txn::whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalFpx = \App\Models\Txn::where('payment_type', 'FPX')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalCC = \App\Models\Txn::where('payment_type', 'Credit Card')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalBoost = \App\Models\Txn::where('payment_type', 'Boost')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalTng = \App\Models\Txn::where('payment_type', 'Touch N Go')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalBasic = \App\Models\Txn::whereIn('package_id', $basic_packages)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalAdvanced = \App\Models\Txn::whereIn('package_id', $advanced_packages)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalSpecial = \App\Models\Txn::whereIn('package_id', $special_packages)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalAffiliate = \App\Models\Txn::where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonhtlyTotalNonAffiliate = \App\Models\Txn::where('is_from_affiliate', 0)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->count();
        $signupMonthlyAmount = \App\Models\Txn::whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmount = str_replace(",", "", $signupMonthlyAmount);
        $signupMonthlyAmountBasic = \App\Models\Txn::whereIn('package_id', $basic_packages)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountBasic = str_replace(",", "", $signupMonthlyAmountBasic);
        $signupMonthlyAmountAdvanced = \App\Models\Txn::whereIn('package_id', $advanced_packages)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountAdvanced = str_replace(",", "", $signupMonthlyAmountAdvanced);
        $signupMonthlyAmountSpecial = \App\Models\Txn::whereIn('package_id', $special_packages)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountSpecial = str_replace(",", "", $signupMonthlyAmountSpecial);
        $signupMonthlyAmountAffiliate = \App\Models\Txn::where('is_from_affiliate', 1)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountAffiliate = str_replace(",", "", $signupMonthlyAmountAffiliate);
        $signupMonthlyAmountNonAffiliate = \App\Models\Txn::where('is_from_affiliate', 0)->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountNonAffiliate = str_replace(",", "", $signupMonthlyAmountNonAffiliate);
        $signupMonthlyAmountCC = \App\Models\Txn::where('payment_type', 'Credit Card')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountCC = str_replace(",", "", $signupMonthlyAmountCC);
        $signupMonthlyAmountFpx = \App\Models\Txn::where('payment_type', 'FPX')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountFpx = str_replace(",", "", $signupMonthlyAmountFpx);
        $signupMonthlyAmountBoost = \App\Models\Txn::where('payment_type', 'Boost')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountBoost = str_replace(",", "", $signupMonthlyAmountBoost);
        $signupMonthlyAmountTng = \App\Models\Txn::where('payment_type', 'Touch N Go')->whereMonth('created_at', '=', $m)->whereYear('created_at', '=', $y)->sum('price_total');
        $signupMonthlyAmountTng = str_replace(",", "", $signupMonthlyAmountTng);

        $dbSignupMonthly = \App\Models\TxnMonthly::where('month', '=', $m)->where('year', '=', $y)->first();
        if (!$dbSignupMonthly)
        {
            $dbSignupMonthly = new \App\Models\TxnMonthly;
        }
        $prevMonth = date('m', strtotime('first day of previous month'));
        $year = date('Y', strtotime('first day of previous month'));
        $prev = \App\Models\TxnMonthly::where('month', '=', $prevMonth)->where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupMonthly->total_status = $prevTotal > $signupMonhtlyTotal ? 'down' : 'up';
        $dbSignupMonthly->total_amount_status = $prevTotalAmount > $signupMonthlyAmount ? 'down' : 'up';
        $dbSignupMonthly->total_status_percent = number_format(($signupMonhtlyTotal - $prevTotal) / $this->checkamount($signupMonhtlyTotal) * 100, 0);
        $dbSignupMonthly->total_amount_status_percent = number_format(($signupMonthlyAmount - $prevTotalAmount) / $this->checkamount($signupMonthlyAmount) * 100, 0);

        $dbSignupMonthly->total = $signupMonhtlyTotal;
        $dbSignupMonthly->total_fpx = $signupMonhtlyTotalFpx;
        $dbSignupMonthly->total_cc = $signupMonhtlyTotalCC;
        $dbSignupMonthly->total_boost = $signupMonhtlyTotalBoost;
        $dbSignupMonthly->total_tng = $signupMonhtlyTotalTng;
        $dbSignupMonthly->total_basic = $signupMonhtlyTotalBasic;
        $dbSignupMonthly->total_advanced = $signupMonhtlyTotalAdvanced;
        $dbSignupMonthly->total_special = $signupMonhtlyTotalSpecial;
        $dbSignupMonthly->total_affiliate = $signupMonhtlyTotalAffiliate;
        $dbSignupMonthly->total_nonaffiliate = $signupMonhtlyTotalNonAffiliate;
        $dbSignupMonthly->month = $m;
        $dbSignupMonthly->year = $y;
        $dbSignupMonthly->total_amount = number_format($signupMonthlyAmount, 2);
        $dbSignupMonthly->total_amount_fpx = number_format($signupMonthlyAmountFpx, 2);
        $dbSignupMonthly->total_amount_cc = number_format($signupMonthlyAmountCC, 2);
        $dbSignupMonthly->total_amount_boost = number_format($signupMonthlyAmountBoost, 2);
        $dbSignupMonthly->total_amount_tng = number_format($signupMonthlyAmountTng, 2);
        $dbSignupMonthly->total_amount_basic = number_format($signupMonthlyAmountBasic, 2);
        $dbSignupMonthly->total_amount_advanced = number_format($signupMonthlyAmountAdvanced, 2);
        $dbSignupMonthly->total_amount_special = number_format($signupMonthlyAmountSpecial, 2);
        $dbSignupMonthly->total_amount_affiliate = number_format($signupMonthlyAmountAffiliate, 2);
        $dbSignupMonthly->total_amount_nonaffiliate = number_format($signupMonthlyAmountNonAffiliate, 2);
        $dbSignupMonthly->save();
    }

    private function txnyearly()
    {
        $signupYearlyTotal = \App\Models\Txn::whereYear('created_at', '=', date('Y'))->count();
        $signupYearlyAmount = \App\Models\Txn::whereYear('created_at', '=', date('Y'))->sum('price_total');
        $signupYearlyAmount = str_replace(",", "", $signupYearlyAmount);

        $dbSignupYearly = \App\Models\TxnYearly::where('year', '=', date('Y'))->first();
        if (!$dbSignupYearly)
        {
            $dbSignupYearly = new \App\Models\TxnYearly;
        }
        $year = date('Y', strtotime('first day of previous year'));
        $prev = \App\Models\TxnYearly::where('year', $year)->first();
        if ($prev)
        {
            $prevTotal = $prev->total;
            $prevTotalAmount = $prev->total_amount;
            $prevTotalAmount = str_replace(",", "", $prevTotalAmount);
        }
        else
        {
            $prevTotal = 0;
            $prevTotalAmount = 0;
        }

        $dbSignupYearly->total_status = $prevTotal > $signupYearlyTotal ? 'down' : 'up';
        $dbSignupYearly->total_amount_status = $prevTotalAmount > $signupYearlyAmount ? 'down' : 'up';
        $dbSignupYearly->total_status_percent = number_format(($signupYearlyTotal - $prevTotal) / $this->checkamount($signupYearlyTotal) * 100, 0);
        $dbSignupYearly->total_amount_status_percent = number_format(($signupYearlyAmount - $prevTotalAmount) / $this->checkamount($signupYearlyAmount) * 100, 0);

        $dbSignupYearly->total = $signupYearlyTotal;
        $dbSignupYearly->year = date('Y');
        $dbSignupYearly->total_amount = number_format($signupYearlyAmount, 2);
        $dbSignupYearly->save();
    }

    private function checkamount($amount)
    {
        if ($amount == 0)
        {
            return 1.00;
        }
        else
        {
            return $amount;
        }
    }

}
