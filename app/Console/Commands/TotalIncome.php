<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class TotalIncome extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:income';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $year = date('Y');

        for ($i = 1; $i <= 12; $i++) {
            $month = sprintf("%02d", $i);
            $mdrCc = \App\Models\MdrCc::where('month', $month)->where('year', $year)->first();
            $mdrFpx = \App\Models\MdrFpx::where('month', $month)->where('year', $year)->first();

            #total boost n TnG mdr
            $allTxnsEwallet = \App\Models\Txn::select(DB::raw("SUM(price_total)*(1.5/100) as total"))->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)
                    ->where('status_id', 4)
                    ->where(function($q) {
                        $q->orWhere('payment_type', 'Boost')
                        ->orWhere('payment_type', 'Touch N Go')
                        ->orWhere('payment_type', 'GrabPay');
                    })
                    ->first();

            $fees = \App\Models\ProcessingFee::where('month', $month)->where('year', $year)->first();

            $totalSignup = \App\Models\Signup::select(DB::raw("SUM(value) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->where('type', 'registration')->first();

            $totalRenew = \App\Models\Signup::select(DB::raw("SUM(value) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->where('type', 'renewal')->first();

            if ($mdrCc && $mdrFpx && $totalSignup && $totalRenew && $allTxnsEwallet)
                $totalIncome = $mdrCc->net_income + $mdrFpx->net_income + $totalSignup->sum + $totalRenew->sum + $allTxnsEwallet->total + ($fees ? $fees->total : 0);
            else
                $totalIncome = 0;

            $total = \App\Models\TotalIncome::where('month', $month)->where('year', $year)->first();

            if (!$total) {
                $total = new \App\Models\TotalIncome;
            }

            $total->month = $month;
            $total->year = $year;
            $total->total = $totalIncome;
            $total->save();
        }
    }

}
