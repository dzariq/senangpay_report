<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TotalIncome extends Model
{

    protected $table = 'report_total_income';
    public $timestamps = false;


}
