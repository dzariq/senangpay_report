<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxnMonthly extends Model
{

    protected $table = 'report_tx_monthly';
    public $timestamps = false;


}
