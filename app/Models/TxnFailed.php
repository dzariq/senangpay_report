<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxnFailed extends Model
{

    protected $table = 'senangpay_tx_failed';
    public $timestamps = false;


}
