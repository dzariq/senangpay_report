<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxnAllTime extends Model
{

    protected $table = 'report_tx_alltime';
    public $timestamps = false;


}
