<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateMonthly extends Model
{

    protected $table = 'report_affiliate_monthly';
    public $timestamps = false;


}
