<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TerminateYearly extends Model
{

    protected $table = 'report_terminate_yearly';
    public $timestamps = false;


}
