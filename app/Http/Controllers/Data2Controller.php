<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Log;

class Data2Controller extends Controller
{

    protected $zabbix;

    public function __construct()
    {
        
    }

    public function index()
    {
       

//        $signupData = $rawData->signup;
        $txnData = json_decode(file_get_contents('october_txn.json'));
      $signupData = json_decode(file_get_contents('october_signup.json'));
        //print_r($txnData);die;
        foreach ($signupData as $data)
        {
            $signup = \App\Models\Signup::find($data->id);
            if(!$signup){
                $signup = new \App\Models\Signup;
            }
            $signup->id = $data->id;
            $signup->value = $data->amount;
            $signup->package_id = $data->package_id;
            $signup->merchant_name = $data->merchant_name;
            $signup->created_at = $data->date_created;
            $signup->type = $data->type;
            $signup->status_id = $data->status_id;
            $signup->payment_type = $data->payment_type;
            $signup->is_from_affiliate = $data->is_from_affiliate;
            $signup->save();
        }
//        
        foreach ($txnData as $data)
        {
            $signup = \App\Models\Txn::find($data->id);
            if(!$signup){
                $signup = new \App\Models\Txn;
            }
            $signup->id = $data->id;
            $signup->price_total = $data->price_total;
            $signup->customer_name = $data->customer_name;
            $signup->email = $data->email;
            $signup->merchant_id = $data->merchant_id;
            $signup->merchant_name = $data->merchant_name;
            $signup->created_at = $data->date_created;
            $signup->status_id = $data->status_id;
            $signup->payment_type = $data->payment_type;
            $signup->is_from_affiliate = $data->is_from_affiliate;
            $signup->package_id = $data->package_id;
            $signup->save();
        }
    }

}
