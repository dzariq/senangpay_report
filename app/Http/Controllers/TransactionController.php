<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;

class TransactionController extends Controller
{

    protected $zabbix;

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function GET_index()
    {

        $theme = Theme::uses('eye')->layout('default');


//        $theme->asset()->container('post-scripts')->usePath()->add('laravel1', 'js/app.plugin.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel2', 'js/charts/easypiechart/jquery.easy-pie-chart.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel3', 'js/charts/sparkline/jquery.sparkline.min.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel4', 'js/charts/flot/jquery.flot.min.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel5', 'js/charts/flot/jquery.flot.tooltip.min.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel6', 'js/charts/flot/jquery.flot.resize.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel7', 'js/charts/flot/jquery.flot.grow.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel8', 'js/charts/flot/demo.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel9', 'js/calendar/bootstrap_calendar.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel10', 'js/calendar/demo.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel11', 'js/sortable/jquery.sortable.js');



        $theme->asset()->container('post-scripts')->usePath()->add('laravel6', 'js/pages/charts/flot.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel7', 'js/pages/charts/sparkline.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel9', 'js/pages/charts/jquery-knob.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel10', 'js/pages/index2.js');

        $theme->asset()->container('post-scripts')->add('laravel11', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular.min.js');
        $theme->asset()->container('post-scripts')->add('laravel12', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel14', 'js/app.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel15', 'js/pages/charts/angular-chart.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel16', 'js/modules/progress.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel17', 'js/services/chart_signup.service.js');


        $params = array();
        return $theme->scope('report.transaction', $params)->render();
    }

}
