<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Log;

class DataTxnController extends Controller
{

    protected $zabbix;

    public function __construct()
    {
        
    }

    public function index()
    {
        Log::info($_POST['data']);
        $rawData = json_decode($_POST['data']);

        $all_time_txn = $rawData->all_time_txn;
        $all_time_paid = $rawData->all_time_paid;
        $all_time_fpx = $rawData->all_time_fpx;
        $all_time_cc = $rawData->all_time_cc;
        $all_time_boost = $rawData->all_time_boost;
        $all_time_tng = $rawData->all_time_tng;
        $all_time_basic = $rawData->all_time_basic;
        $all_time_advanced = $rawData->all_time_advanced;
        $all_time_special = $rawData->all_time_special;
        $all_time_affiliate = $rawData->all_time_affiliate;
        $all_time_count = $rawData->all_time_count;
        $all_time_unique = $rawData->all_time_unique;
        $all_time_unique_all_status = $rawData->all_time_unique_all_status;

        $TxnAllTime = \App\Models\TxnAllTime::first();
        $TxnAllTime->total = $all_time_txn;
        $TxnAllTime->paid = $all_time_paid;
        $TxnAllTime->fpx = $all_time_fpx;
        $TxnAllTime->cc = $all_time_cc;
        $TxnAllTime->boost = $all_time_boost;
        $TxnAllTime->tng = $all_time_tng;
        $TxnAllTime->basic = $all_time_basic;
        $TxnAllTime->advanced = $all_time_advanced;
        $TxnAllTime->special = $all_time_special;
        $TxnAllTime->affiliate = $all_time_affiliate;
        $TxnAllTime->freq = $all_time_count;
        $TxnAllTime->unique_count_all_status = $all_time_unique_all_status;
        $TxnAllTime->save();
    }

}
