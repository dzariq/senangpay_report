<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Log;

class MdrController extends Controller
{

    protected $zabbix;

    public function __construct()
    {
        
    }

    public function index()
    {
        $rawData = json_decode($_POST['data']);

        $timestamp = $rawData->start;
        
        $month = date('m', $timestamp);
        $year = date('Y', $timestamp);

        $mdr_cc = $rawData->mdr_cc;
        $merchant_mdr_cc = $rawData->merchant_mdr_cc;
        $total_cc_amount = $rawData->total_cc_amount;
        $net_income_cc = $rawData->net_income_cc;
        $mdr_fpx = $rawData->mdr_fpx;
        $merchant_mdr_fpx = $rawData->merchant_mdr_fpx;
        $total_fpx_amount = $rawData->total_fpx_amount;
        $net_income_fpx = $rawData->net_income_fpx;

        $mdrCC = \App\Models\MdrCc::where('month', $month)->where('year', $year)->first();
        $mdrFPX = \App\Models\MdrFpx::where('month', $month)->where('year', $year)->first();

        if (!$mdrCC)
        {
            $mdrCC = new \App\Models\MdrCc;
        }
        $mdrCC->mdr = $mdr_cc;
        $mdrCC->merchant_mdr = $merchant_mdr_cc;
        $mdrCC->total_amount = $total_cc_amount;
        $mdrCC->net_income = $net_income_cc;
        $mdrCC->month = $month;
        $mdrCC->year = $year;
        $mdrCC->save();

        if (!$mdrFPX)
        {
            $mdrFPX = new \App\Models\MdrFpx;
        }
        $mdrFPX->mdr = $mdr_fpx;
        $mdrFPX->merchant_mdr = $merchant_mdr_fpx;
        $mdrFPX->total_amount = $total_fpx_amount;
        $mdrFPX->net_income = $net_income_fpx;
        $mdrFPX->month = $month;
        $mdrFPX->year = $year;
        $mdrFPX->save();
    }

}
