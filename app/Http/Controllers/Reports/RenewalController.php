<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;

class RenewalController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
        
       $daily = $this->getdaily();
        $monthly = $this->getmonthly();
        $yearly = $this->getyearly();

        echo json_encode(array(
            'daily' => $daily,
            'monthly' => $monthly,
            'yearly' => $yearly,
        ));
    }

    protected function getdaily()
    {
        $data = \App\Models\RenewDaily::whereDate('date', '=', date('Y-m-d'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getmonthly()
    {
        $data = \App\Models\RenewMonthly::where('month', '=', date('m'))->where('year', '=', date('Y'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getyearly()
    {
        $data = \App\Models\RenewYearly::where('year', '=', date('Y'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

}
