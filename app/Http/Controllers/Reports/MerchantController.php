<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;

class MerchantController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
        
              $data = \App\Models\Merchant::first();

        $arpu = 0;

        $paid = \App\Models\TxnAllTime::first();
        $activeMerchant = \App\Models\Merchant::first();
        
        $arpu = number_format($paid->paid/$activeMerchant->active,0);

        echo json_encode(array(
            'active' => $data->active,
            'total' => $data->total,
            'basic' => $data->basic,
            'advanced' => $data->advanced,
            'special' => $data->special,
            'arpu' => $arpu,
        ));
    }

}
