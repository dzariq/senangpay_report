<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;

class TxnAlltimeController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
  
        $txn = \App\Models\TxnAllTime::first();
        
        $totalOfBAS = (float)$txn->basic + (float)$txn->advanced + (float)$txn->special;
        $totalOfPt = (float)$txn->fpx + (float)$txn->cc + (float)$txn->boost + (float)$txn->tng;

        echo json_encode(array(
            'total' => $txn->total,
            'paid' => $txn->paid,
            'basic' => number_format(($txn->basic/$totalOfBAS)*100,0),
            'advanced' => number_format(($txn->advanced/$totalOfBAS)*100,0),
            'special' => number_format(($txn->special/$totalOfBAS)*100,0),
            'fpx' => number_format(($txn->fpx/$totalOfPt)*100,0),
            'cc' => number_format(($txn->cc/$totalOfPt)*100,0),
            'boost' => number_format(($txn->boost/$totalOfPt)*100,0),
            'tng' => number_format(($txn->tng/$totalOfPt)*100,0),
            'affiliate' => number_format(($txn->affiliate/$totalOfBAS)*100,0),
            'nonaffiliate' => number_format((($totalOfBAS - $txn->affiliate)/$totalOfBAS)*100,0),
            'unique_count' => $txn->unique_count,
            'unique_all_status' => $txn->unique_count_all_status,
            'avpt' => number_format($txn->paid/$txn->freq,2),
        ));
    }

    

}
