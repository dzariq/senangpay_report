<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;

class ZendeskController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
        
        $thisMonth = date('m');
        $thisYear = date('Y');

        $alltimeClosed = array();
        $dailyAverage = array();

        $datelastmonth = date('Y-m-d', strtotime("-30 days"));



        $todayData = $this->getticket(date('Y-m-d'));
        $thismonthData = $this->getticketthismonth($thisYear . '-' . $thisMonth . '-' . '01');
        $alltimeClosed = $this->alltimeclosed();
        $dailyAverage = $this->new_average_daily($datelastmonth);
       // $metrics = $this->metrics();

        echo json_encode(array(
            'today' => $todayData,
            'thismonth' => $thismonthData,
            'alltime_closed' => $alltimeClosed,
            'new_average_daily' => $dailyAverage,
         //   'metrics' => $metrics
        ));
    }

    protected function getticket($date)
    {
        $new = Zendesk::search()->find('type:ticket status<closed created:' . $date);
        $pending = Zendesk::search()->find('type:ticket status:pending created:' . $date);
        $solved = Zendesk::search()->find('type:ticket status:solved solved:' . $date);

        return array(
            'new' => ($new->count),
            'pending' => ($pending->count),
            'solved' => ($solved->count),
        );
    }

    protected function getticketthismonth($date)
    {
        $new = Zendesk::search()->find('type:ticket status<closed created>=' . $date);
        $pending = Zendesk::search()->find('type:ticket status:pending created>=' . $date);
        $solved = Zendesk::search()->find('type:ticket status:solved solved>=' . $date);

        return array(
            'new' => ($new->count),
            'pending' => ($pending->count),
            'solved' => ($solved->count),
        );
    }

    protected function alltimeclosed()
    {
        $new = Zendesk::search()->find('type:ticket status:closed');

        return array(
            'data' => ($new->count),
        );
    }

    protected function new_average_daily($date)
    {
        $new = Zendesk::search()->find('type:ticket status<closed created>=' . $date);


        return array(
            'data' => ceil($new->count/30),
        );
    }

    protected function metrics()
    {
        $new = Zendesk::TicketMetrics()->find('type:ticket status<closed created>=' . $date);


        return array(
            'data' => ceil($new->count/30),
        );
    }

}
