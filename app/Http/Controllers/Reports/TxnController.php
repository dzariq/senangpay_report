<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;

class TxnController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
        
       $daily = $this->getdaily();
        $monthly = $this->getmonthly();
        $yearly = $this->getyearly();

        //get last  1 minute txn total
        $lasttxn = \App\Models\Txn::orderby('id', 'desc')->first();
        $lasttxntime = $lasttxn->created_at;
        $time = strtotime($lasttxntime);
        $time = $time - (5 * 60);
        $start_timestamp2 = date("Y-m-d H:i:s", $time);
        $start_timestamp = date('Y-m-d H:i:s', strtotime('-1 minutes'));
        $end_timestamp = date('Y-m-d H:i:s', strtotime('now'));

        $dateToday = date('d');
        $monthAmount = str_replace(",", "", $monthly['total_amount']);
        $apm = number_format($monthAmount / $dateToday / 24 / 60, 2);
        $last5mintotal = \App\Models\Txn::where('created_at', '>=', $start_timestamp2)->where('created_at', '<', $end_timestamp)->count();
        $lastfivemintxn = \App\Models\Txn::orderby('id', 'DESC')->limit(5)->get();

        $lastfivemintxn = $lastfivemintxn->toArray();
        foreach ($lastfivemintxn as &$item)
        {
            $item['created_at'] = date('H:i', strtotime($item['created_at']));
        }

        echo json_encode(array(
            'daily' => $daily,
            'monthly' => $monthly,
            'yearly' => $yearly,
            'apm' => $apm,
            'lastfivemin' => $lastfivemintxn ? $lastfivemintxn : array(),
            'lastfivemintotal' => $last5mintotal,
        ));
    }

    protected function getdaily()
    {
        $data = \App\Models\TxnDaily::whereDate('date', '=', date('Y-m-d'))->first();
        $dataYesteday = \App\Models\TxnDaily::whereDate('date', '=', date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount' => $data->total_amount,
            'total_amount_yesterday' => $dataYesteday->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getmonthly()
    {
        $data = \App\Models\TxnMonthly::where('month', '=', date('m'))->where('year', '=', date('Y'))->first();
        $lastMonth = \App\Models\TxnMonthly::where('month', '=', date("m", strtotime("-1 months")))->where('year', '=', date("Y", strtotime("-1 months")))->first();

        $data->total_amount_basic = str_replace(",", "", $data->total_amount_basic);
        $data->total_amount_advanced = str_replace(",", "", $data->total_amount_advanced);
        $data->total_amount_special = str_replace(",", "", $data->total_amount_special);
        $data->total_amount_cc = str_replace(",", "", $data->total_amount_cc);
        $data->total_amount_fpx = str_replace(",", "", $data->total_amount_fpx);
        $data->total_amount_boost = str_replace(",", "", $data->total_amount_boost);
        $data->total_amount_tng = str_replace(",", "", $data->total_amount_tng);
        $data->total_amount_nonaffiliate = str_replace(",", "", $data->total_amount_nonaffiliate);
        $data->total_amount_affiliate = str_replace(",", "", $data->total_amount_affiliate);
                
        $total_bas = $data->total_amount_basic + $data->total_amount_advanced + $data->total_amount_special;
        $total_paymenttype = $data->total_amount_fpx + $data->total_amount_cc + $data->total_amount_boost + $data->total_amount_tng;
        $total_aff = $data->total_amount_affiliate + $data->total_amount_nonaffiliate;

        return array(
            'total' => $data->total,
            'total_lastmonth' => $lastMonth->total,
            'total_basic' => $data->total_basic,
            'total_amount_basic' => number_format((str_replace(",", "", $data->total_amount_basic)/str_replace(",", "", $total_bas))*100,0),
            'total_advanced' => $data->total_advanced,
            'total_amount_advanced' => number_format((str_replace(",", "", $data->total_amount_advanced)/str_replace(",", "", $total_bas))*100,0),
            'total_special' => $data->total_special,
            'total_amount_special' => number_format((str_replace(",", "", $data->total_amount_special)/str_replace(",", "", $total_bas))*100,0),
            'total_fpx' => $data->total_fpx,
            'total_amount_fpx' => number_format((str_replace(",", "", $data->total_amount_fpx)/str_replace(",", "", $total_paymenttype))*100,0),
            'total_cc' => $data->total_cc,
            'total_boost' => $data->total_boost,
            'total_tng' => $data->total_tng,
            'total_amount_cc' => number_format((str_replace(",", "", $data->total_amount_cc)/str_replace(",", "", $total_paymenttype))*100,0),
            'total_amount_boost' => number_format((str_replace(",", "", $data->total_amount_boost)/str_replace(",", "", $total_paymenttype))*100,0),
            'total_amount_tng' => number_format((str_replace(",", "", $data->total_amount_tng)/str_replace(",", "", $total_paymenttype))*100,0),
            'total_affiliate' => $data->total_affiliate,
            'total_affiliate' => str_replace(",", "", $data->total_affiliate),
            'total_nonaffiliate' => $data->total_nonaffiliate,
            'total_amount_nonaffiliate' => number_format((str_replace(",", "", $data->total_amount_nonaffiliate)/str_replace(",", "", $total_aff))*100,0),
            'total_amount_affiliate' => number_format((str_replace(",", "", $data->total_amount_affiliate)/str_replace(",", "", $total_aff))*100,0),
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getyearly()
    {
        $data = \App\Models\TxnYearly::where('year', '=', date('Y'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

}
