<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;
use DB;

class SignupController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
        
       $daily = $this->getdaily();
        $monthly = $this->getmonthly();
        $yearly = $this->getyearly();

        echo json_encode(array(
            'daily' => $daily,
            'monthly' => $monthly,
            'yearly' => $yearly,
        ));
    }

    protected function getdaily()
    {
        $data = \App\Models\SignupDaily::whereDate('date', '=', date('Y-m-d'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getmonthly()
    {
        $data = \App\Models\SignupMonthly::where('month', '=', date('m'))->where('year', '=', date('Y'))->first();

        $allpackage = $data->total_basic + $data->total_advanced + $data->total_special;

        return array(
            'total' => $data->total,
            'total_basic' => ($data->total_basic),
            'total_advanced' => ($data->total_advanced ),
            'total_special' => ($data->total_special),
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getyearly()
    {
        $data = \App\Models\SignupYearly::where('year', '=', date('Y'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    private function get_package_array_given_tier($tier)
    {
        $packages = \App\Models\Package::where('tier', $tier)->where('is_public', 1)->where('type', 'yearly')->get();

        return $packages->pluck('id');
    }

    public function getmonthlybyyear()
    {
        $basic_packages = $this->get_package_array_given_tier(1);
        $advanced_packages = $this->get_package_array_given_tier(2);
        $special_packages = $this->get_package_array_given_tier(3);

        //set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        $type = '';
        $package = '';
        $affiliate = '';
        $category = '';
        $payment_type = '';

        if (isset($_GET['type']))
        {
            $type = $_GET['type'];
        }

        if (isset($_GET['package']))
        {
            $package = $_GET['package'];
        }

        if (isset($_GET['affiliate']))
        {
            $affiliate = $_GET['affiliate'];
        }

        if (isset($_GET['category']))
        {
            $category = $_GET['category'];
        }

        if (isset($_GET['payment_type']))
        {
            $payment_type = $_GET['payment_type'];
        }

        if (isset($_GET['to_year']))
        {
            $to_year = $_GET['to_year'];
        }
        else
        {
            $to_year = date('Y');
        }

        if (isset($_GET['from_year']))
        {
            $from_year = $_GET['from_year'];
        }
        else
        {
            $from_year = date('Y');
        }

        if (isset($_GET['from_month']))
        {
            $from_month = $_GET['from_month'];
        }
        else
        {
            $from_month = 1;
        }

        if (isset($_GET['to_month']))
        {
            $to_month = $_GET['to_month'];
        }
        else
        {
            $to_month = 12;
        }

        $item = array();

        if ($from_year != $to_year)
        {
            $last_month = (12 - $from_month) + 1 + $to_month;
        }
        else
        {
            $last_month = $to_month;
        }
        $count = 0;
        for ($i = 1; $i <= $last_month; $i++)
        {
            $month = $from_month;
            if ($month > 12)
            {
                $month = $count + 1;
                $year_to_query = $from_year + 1;
                $count++;
            }
            else
            {
                $year_to_query = $from_year;
            }

            if ($month < 10)
            {
                $month = '0' . $month;
            }
//           echo $month . '-' . $year_to_query . '==';
            if ($category == 'signup')
            {
                if ($type == 'count')
                    $data = \App\Models\Signup::select(DB::raw("COUNT(*) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month)->where('type', 'registration');
                else
                    $data = \App\Models\Signup::select(DB::raw("SUM(value) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month)->where('type', 'registration');
            }
            else if ($category == 'renewal')
            {
                if ($type == 'count')
                    $data = \App\Models\Signup::select(DB::raw("COUNT(*) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month)->where('type', 'renewal');
                else
                    $data = \App\Models\Signup::select(DB::raw("SUM(value) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month)->where('type', 'renewal');
            }
            else if ($category == 'refund')
            {
                if ($type == 'count')
                    $data = \App\Models\Refund::select(DB::raw("COUNT(*) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
                else
                    $data = \App\Models\Refund::select(DB::raw("SUM(amount) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
            }
            else if ($category == 'affiliate')
            {
                if ($type == 'count')
                    $data = \App\Models\Affiliate::select(DB::raw("COUNT(*) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
                else
                    $data = \App\Models\Affiliate::select(DB::raw("SUM(amount) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
            }
            else if ($category == 'mdr_cc')
            {
                $mdr = \App\Models\MdrCc::select(DB::raw("mdr"))->where('year', '=', $year_to_query)->where('month', '=', $month);
                $mdr_merchant = \App\Models\MdrCc::select(DB::raw("merchant_mdr"))->where('year', '=', $year_to_query)->where('month', '=', $month);
                $net_income = \App\Models\MdrCc::select(DB::raw("net_income"))->where('year', '=', $year_to_query)->where('month', '=', $month);
                $total_amount = \App\Models\MdrCc::select(DB::raw("total_amount"))->where('year', '=', $year_to_query)->where('month', '=', $month);
            }
            else if ($category == 'mdr_fpx')
            {
                $mdr = \App\Models\MdrFpx::select(DB::raw("mdr"))->where('year', '=', $year_to_query)->where('month', '=', $month);
                $mdr_merchant = \App\Models\MdrFpx::select(DB::raw("merchant_mdr"))->where('year', '=', $year_to_query)->where('month', '=', $month);
                $net_income = \App\Models\MdrFpx::select(DB::raw("net_income"))->where('year', '=', $year_to_query)->where('month', '=', $month);
                $total_amount = \App\Models\MdrFpx::select(DB::raw("total_amount"))->where('year', '=', $year_to_query)->where('month', '=', $month);
            }
            else if ($category == 'income')
            {
                $totalIncome = \App\Models\TotalIncome::select(DB::raw("total"))->where('year', '=', $year_to_query)->where('month', '=', $month);
            }
            else if ($category == 'processing_fee')
            {
                $processingFee = \App\Models\ProcessingFee::select(DB::raw("total"))->where('year', '=', $year_to_query)->where('month', '=', $month);
            }
            else if($category == 'transaction_failed')
            {
                if ($type == 'count')
                    $data = \App\Models\TxnFailed::select(DB::raw("COUNT(*) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
                else
                    $data = \App\Models\TxnFailed::select(DB::raw("SUM(price_total) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
            }
            else
            {
                if ($type == 'count')
                    $data = \App\Models\Txn::select(DB::raw("COUNT(*) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
                else
                    $data = \App\Models\Txn::select(DB::raw("SUM(price_total) as sum,YEAR(created_at) as year, MONTH(created_at) as month"))->whereYear('created_at', '=', $year_to_query)->whereMonth('created_at', '=', $month);
            }

            if ($affiliate != '' && $affiliate == 'affiliate')
                $data = $data->where(function ($q)
                {
                    $q->where('is_from_affiliate', 1);
                });

            if ($affiliate != '' && $affiliate == 'non_affiliate')
                $data = $data->where(function ($q)
                {
                    $q->where('is_from_affiliate', 0);
                });


            if ($payment_type != '')
            {
                $data = $data->where(function ($q) use ($payment_type)
                {
                    $q->where('payment_type', $payment_type);
                });
            }

            if ($package != '' && $package == 'basic')
                $data = $data->whereIn('package_id', $basic_packages);

            if ($package != '' && $package == 'advanced')
                $data = $data->whereIn('package_id', $advanced_packages);

            if ($package != '' && $package == 'special')
                $data = $data->whereIn('package_id', $special_packages);

            if ($package != '' && $package == 'monthly')
            {
                $data = $data->where(function ($q)
                {
                    $q->where('package_id', 1);
                });
            }

            if ($category == 'mdr_cc' || $category == 'mdr_fpx')
            {
                $mdr = $mdr->groupby(DB::raw("month"))->groupby(DB::raw("year"))->get();
                $mdr_merchant = $mdr_merchant->groupby(DB::raw("month"))->groupby(DB::raw("year"))->get();
                $total_amount = $total_amount->groupby(DB::raw("month"))->groupby(DB::raw("year"))->get();
                $net_income = $net_income->groupby(DB::raw("month"))->groupby(DB::raw("year"))->get();
                foreach ($mdr->toArray() as $key => $moo)
                {
                    $item[$this->getMonthName($month) . ' ' . $year_to_query] = array(
                        'total_amount' => number_format($total_amount[$key]['total_amount'], 2, '.', ''),
                        'mdr' => number_format($mdr[$key]['mdr'], 2, '.', ''),
                        'merchant_mdr' => number_format($mdr_merchant[$key]['merchant_mdr'], 2, '.', ''),
                        'net_income' => number_format($net_income[$key]['net_income'], 2, '.', ''),
                    );
                }
            }
            else if ($category == 'income')
            {
                $totalIncome = $totalIncome->groupby(DB::raw("month"))->groupby(DB::raw("year"))->get();
                foreach ($totalIncome->toArray() as $key => $moo)
                {
                    $item[$this->getMonthName($month) . ' ' . $year_to_query] = array(
                        'total_amount' => number_format($totalIncome[$key]['total'], 2, '.', ''),
                    );
                }
            }
            else if ($category == 'processing_fee')
            {
                $processingFee = $processingFee->groupby(DB::raw("month"))->groupby(DB::raw("year"))->get();
                foreach ($processingFee->toArray() as $key => $moo)
                {
                    $item[$this->getMonthName($month) . ' ' . $year_to_query] = array(
                        'total_amount' => number_format($processingFee[$key]['total'], 2, '.', ''),
                    );
                }
            }
            else
            {
                $data = $data->groupby(DB::raw("MONTH(created_at)"))->groupby(DB::raw("YEAR(created_at)"))->get();

                foreach ($data->toArray() as $moo)
                {

                    $item[$this->getMonthName($month) . ' ' . $year_to_query] = number_format($moo['sum'], 2, '.', '');
                }
            }



            $from_month++;
        }

        echo json_encode($item);
    }

    protected function getMonthName($i)
    {
        if ($i == '01')
        {
            return 'January';
        }
        if ($i == '02')
        {
            return 'February';
        }
        if ($i == '03')
        {
            return 'March';
        }
        if ($i == '04')
        {
            return 'April';
        }
        if ($i == '05')
        {
            return 'May';
        }
        if ($i == '06')
        {
            return 'June';
        }
        if ($i == '07')
        {
            return 'July';
        }
        if ($i == '08')
        {
            return 'August';
        }
        if ($i == '09')
        {
            return 'September';
        }
        if ($i == '10')
        {
            return 'October';
        }
        if ($i == '11')
        {
            return 'November';
        }
        if ($i == '12')
        {
            return 'December';
        }
    }

}
