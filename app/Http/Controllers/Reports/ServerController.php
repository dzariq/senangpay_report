<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Log;

class ServerController extends Controller
{

    protected $zabbix;

    public function __construct()
    {
        $this->zabbix = app('zabbix');
    }

    public function monitoring()
    {
//        Log::info(json_encode($_POST));

        $new_value = new \App\Models\ServerMonitor;
        $new_value->cpu = $_POST['cpu'];
        $new_value->disk = $_POST['disk'];
        $new_value->memory = $_POST['memory'];
        $new_value->server_name = $_POST['server_name'];
        $new_value->save();
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        $past1hour = strtotime('- 1 hour');
        $starttime = date('Y-m-d H:i:s', $past1hour);


        $dataWeb03 = \App\Models\ServerMonitor::where('server_name', 'web03')->limit(10)->get();
        $dataWeb04 = \App\Models\ServerMonitor::where('server_name', 'web04')->limit(10)->get();

        $web_03_disk_usage = array();
        $web_03_cpu = array();
        $web_03_memory = array();

        $web_04_disk_usage = array();
        $web_04_cpu = array();
        $web_04_memory = array();
        
        foreach ($dataWeb03 as $data)
        {
            $web_03_disk_usage[] = $data->disk;
            $web_03_cpu[] = $data->cpu;
            $web_03_memory[] = $data->memory;
        }
        
        foreach ($dataWeb04 as $data)
        {
            $web_04_disk_usage[] = $data->disk;
            $web_04_cpu[] = $data->cpu;
            $web_04_memory[] = $data->memory;
        }

        $web03 = array(
            'name' => 'Web 03',
            'disk' => $web_03_disk_usage,
            'cpu' => $web_03_cpu,
            'memory' => $web_03_memory,
        );

        $web04 = array(
            'name' => 'Web 04',
            'disk' => $web_04_disk_usage,
            'cpu' => $web_04_cpu,
            'memory' => $web_04_memory,
        );

        echo json_encode(
                array(
                    $web03, $web04)
        );
    }

  
}
