<?php

namespace App\Http\Controllers\Reports;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Zendesk;

class AffiliateController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
//set headers to NOT cache a page
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //
        
       $daily = $this->getdaily();
        $monthly = $this->getmonthly();
        $yearly = $this->getyearly();
        $alltime = $this->getalltime();
        $average = $this->getaverage();

        echo json_encode(array(
            'daily' => $daily,
            'monthly' => $monthly,
            'yearly' => $yearly,
            'alltime' => $alltime,
            'average' => $average,
        ));
    }

    protected function getdaily()
    {
        $data = \App\Models\AffiliateDaily::whereDate('date', '=', date('Y-m-d'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getmonthly()
    {
        $data = \App\Models\AffiliateMonthly::where('month', '=', date('m'))->where('year', '=', date('Y'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getyearly()
    {
        $data = \App\Models\AffiliateYearly::where('year', '=', date('Y'))->first();

        return array(
            'total' => $data->total,
            'total_status' => $data->total_status,
            'total_amount' => $data->total_amount,
            'total_amount_status' => $data->total_amount_status,
            'total_status_percent' => $data->total_status_percent,
            'total_amount_status_percent' => $data->total_amount_status_percent
        );
    }

    protected function getalltime()
    {
        $merchant_affiliate = \App\Models\Affiliate::count();
        $registered_by_affiliate = \App\Models\Signup::where('is_from_affiliate', 1)->where('type', 'registration')->count();
        $signup_value = \App\Models\Signup::where('is_from_affiliate', 1)->where('type', 'registration')->sum('value');
        $txn_value = \App\Models\Txn::where('is_from_affiliate', 1)->sum('price_total');

        $txn_value_fpx = \App\Models\Txn::where('is_from_affiliate', 1)->where('payment_type', 'FPX')->sum('price_total');
        $txn_value_cc = \App\Models\Txn::where('is_from_affiliate', 1)->where('payment_type', 'Credit Card')->sum('price_total');
       
        $txn_value_basic = \App\Models\Txn::where(function($q)
                {
                    $q->where('package_id', 6)
                            ->orWhere('package_id', 11);
                })->where('is_from_affiliate', 1)->sum('price_total');
        $txn_value_advanced = \App\Models\Txn::where(function($q)
                {
                    $q->where('package_id', 7);
                })->where('is_from_affiliate', 1)->sum('price_total');
        $txn_value_special = \App\Models\Txn::where(function($q)
                {
                    $q->where('package_id', 10);
                })->where('is_from_affiliate', 1)->sum('price_total');



        return array(
            'merchant_affiliate' => $merchant_affiliate,
            'registered_by_affiliate' => $registered_by_affiliate,
            'signup_value' => $signup_value,
            'transaction_value' => $txn_value,
            'txn_value_fpx' => number_format($txn_value_fpx / $txn_value * 100, 2),
            'txn_value_cc' => number_format($txn_value_cc / $txn_value * 100, 2),
            'txn_value_basic' => number_format($txn_value_basic / $txn_value * 100, 2),
            'txn_value_advanced' => number_format($txn_value_advanced / $txn_value * 100, 2),
            'txn_value_special' => number_format($txn_value_special / $txn_value * 100, 2),
        );
    }

    protected function getaverage()
    {
        $merchant_affiliate = \App\Models\Affiliate::count();
        $signup_value = \App\Models\Signup::where('is_from_affiliate', 1)->sum('value');
        $txn_value = \App\Models\Txn::where('is_from_affiliate', 1)->sum('price_total');
        $merchant_affiliate_last_month = \App\Models\Affiliate::whereMonth('created_at', '!=', date('m'))->count();
        $signup_value_last_month = \App\Models\Signup::where('is_from_affiliate', 1)->whereMonth('created_at', '!=', date('m'))->sum('value');
        $txn_value_last_month = \App\Models\Txn::where('is_from_affiliate', 1)->whereMonth('created_at', '!=', date('m'))->sum('price_total');


        $d1 = strtotime("2018-10-01");
        $d2 = strtotime('now');
        $min_date = min($d1, $d2);
        $max_date = max($d1, $d2);
        $i = 0;

        while (($min_date = strtotime("+1 MONTH", $min_date)) <= $max_date)
        {
            $i++;
        }
        $months_affiliate = $i; // 8

        $merchant_affiliate = $merchant_affiliate / $months_affiliate;
        $signup_value = $signup_value / $months_affiliate;
        $txn_value = $txn_value / $months_affiliate;

        $signup_value_last_month = $signup_value_last_month / $months_affiliate - 1;
        $transaction_value_last_month = $txn_value_last_month / $months_affiliate - 1;
        $merchant_affiliate_last_month = $merchant_affiliate_last_month / $months_affiliate - 1;

        $signup_status = $signup_value / $signup_value_last_month;
        if ($signup_status < 0)
        {
            $signup_status = 'down';
        }
        else
        {
            $signup_status = 'up';
        }
        $transaction_status = $txn_value / $transaction_value_last_month;
        if ($transaction_status < 0)
        {
            $transaction_status = 'down';
        }
        else
        {
            $transaction_status = 'up';
        }
        $merchant_aff_status = $merchant_affiliate / $merchant_affiliate_last_month;
        if ($merchant_aff_status < 0)
        {
            $merchant_aff_status = 'down';
        }
        else
        {
            $merchant_aff_status = 'up';
        }

        return array(
            'merchant_affiliate' => $merchant_affiliate,
            'signup_value' => $signup_value,
            'transaction_value' => $txn_value,
            'signup_value_last_month' => $signup_value_last_month,
            'transaction_value_last_month' => $transaction_value_last_month,
            'signup_value_percentage_status' => $signup_status,
            'merchant_affiliate_percentage_status' => $merchant_aff_status,
            'merchant_affiliate_percentage' => ($merchant_affiliate / $merchant_affiliate_last_month) * 100,
            'transaction_value_percentage_status' => $transaction_status,
            'signup_value_percentage' => ($signup_value / $signup_value_last_month) * 100,
            'transaction_value_percentage' => ($txn_value / $transaction_value_last_month) * 100,
        );
    }

}
