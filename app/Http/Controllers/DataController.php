<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;
use Log;

class DataController extends Controller {

    protected $zabbix;

    public function __construct() {
        
    }

    public function index() {
        $rawData = json_decode($_POST['data']);
        Log::info('TOTAL TXNS: ' . count($rawData->txn));

        $signupData = $rawData->signup;
        if (isset($rawData->txn)) {
            $txnData = $rawData->txn;
        }
        if (isset($rawData->txn_failed)) {
            $txnDataFailed = $rawData->txn_failed;
        }
        $merchant_active = $rawData->merchant_active;
        $merchant_suspended = $rawData->merchant_suspended;
        $merchant_inactive = $rawData->merchant_inactive;
        $merchant_basic = $rawData->merchant_basic;
        $merchant_advanced = $rawData->merchant_advanced;
        $merchant_special = $rawData->merchant_special;
        $processing_fee = $rawData->processing_fee;

        foreach ($signupData as $data) {
            if (isset($data->merchant_name)) {
                $signup = \App\Models\Signup::find($data->id);
                if (!$signup) {
                    $signup = new \App\Models\Signup;
                }
                $signup->id = $data->id;
                $signup->value = $data->amount;
                $signup->package_id = $data->package_id;
                $signup->merchant_name = $data->merchant_name;
                $signup->created_at = $data->date_created;
                $signup->type = $data->type;
                $signup->status_id = $data->status_id;
                $signup->payment_type = $data->payment_type;
                $signup->is_from_affiliate = $data->is_from_affiliate;
                $signup->package_id = $data->package_id;
                $signup->save();
            }
        }
        if (isset($rawData->txn)) {
            Log::info('all txns: ');

            foreach ($txnData as $data) {
                Log::info('ID: '.$data->id);

                if ($data->id == 12569615) {
                    Log::info('ID: ' . json_encode($data));
                }

                $signup = \App\Models\Txn::find($data->id);
                if (!$signup) {
                    $signup = new \App\Models\Txn;
                }
                $signup->id = $data->id;
                $signup->package_id = $data->package_id;
                $signup->is_from_affiliate = $data->is_from_affiliate;
                $signup->price_total = $data->price_total;
                $signup->customer_name = $data->customer_name;
                $signup->email = $data->email;
                $signup->merchant_id = $data->merchant_id;
                $signup->merchant_name = $data->merchant_name;
                $signup->created_at = $data->date_created;
                $signup->status_id = $data->status_id;
                $signup->payment_type = $data->payment_type;
                $signup->save();
            }
        }
        if (isset($rawData->txn_failed)) {
            foreach ($txnDataFailed as $data) {
                $signup = \App\Models\Txn::find($data->id);
                if (!$signup) {
                    $signup = new \App\Models\TxnFailed;
                }
                $signup->id = $data->id;
                $signup->package_id = $data->package_id;
                $signup->is_from_affiliate = $data->is_from_affiliate;
                $signup->price_total = $data->price_total;
                $signup->customer_name = $data->customer_name;
                $signup->email = $data->email;
                $signup->merchant_id = $data->merchant_id;
                $signup->merchant_name = $data->merchant_name;
                $signup->created_at = $data->date_created;
                $signup->status_id = $data->status_id;
                $signup->payment_type = $data->payment_type;
                $signup->save();
            }
        }

        $merchantReport = \App\Models\Merchant::first();
        $merchantReport->active = $merchant_active;
        $merchantReport->basic = $merchant_basic;
        $merchantReport->advanced = $merchant_advanced;
        $merchantReport->special = $merchant_special;
        $merchantReport->suspended = $merchant_suspended;
        $merchantReport->inactive = $merchant_inactive;
        $merchantReport->total = $merchant_inactive + $merchant_active + $merchant_active;
        $merchantReport->save();

        if ($processing_fee != '') {
            $fee = \App\Models\ProcessingFee::where('month', $processing_fee->month)->where('year', $processing_fee->year)->first();
            if (!$fee)
                $fee = new \App\Models\ProcessingFee;

            $fee->total = $processing_fee->total;
            $fee->month = $processing_fee->month;
            $fee->year = $processing_fee->year;
            $fee->save();
        }
    }

    public function refund() {
        //  Log::info($_POST);
        $rawData = json_decode($_POST['data']);

        foreach ($rawData as $data) {
            $refund = \App\Models\Refund::find($data->id);
            if (!$refund) {
                $refund = new \App\Models\Refund;
            }
            $refund->id = $data->id;
            $refund->amount = $data->amount;
            $refund->fee = $data->fee;
            $refund->created_at = $data->date_created;
            $refund->status_id = $data->status_id;
            $refund->chargeback = $data->chargeback;
            $refund->is_from_affiliate = $data->is_from_affiliate;
            $refund->package_id = $data->package_id;
            $refund->save();
        }
    }

    public function affiliate() {
        //  Log::info($_POST);
        $rawData = json_decode($_POST['data']);

        foreach ($rawData as $data) {
            $affiliate = \App\Models\Affiliate::find($data->id);
            if (!$affiliate) {
                $affiliate = new \App\Models\Affiliate;
            }
            $affiliate->id = $data->id;
            $affiliate->merchant_id = $data->merchant_id;
            $affiliate->commision = $data->commision;
            $affiliate->created_at = $data->date_created;
            $affiliate->save();
        }
    }

}
