<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Theme;
use Helper;

class AdminController extends Controller
{

    protected $zabbix;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function GET_index()
    {

        $theme = Theme::uses('eye')->layout('default');
        $theme->setMenu('admin.index');


//        $theme->asset()->container('post-scripts')->usePath()->add('laravel1', 'js/app.plugin.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel2', 'js/charts/easypiechart/jquery.easy-pie-chart.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel3', 'js/charts/sparkline/jquery.sparkline.min.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel4', 'js/charts/flot/jquery.flot.min.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel5', 'js/charts/flot/jquery.flot.tooltip.min.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel6', 'js/charts/flot/jquery.flot.resize.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel7', 'js/charts/flot/jquery.flot.grow.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel8', 'js/charts/flot/demo.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel9', 'js/calendar/bootstrap_calendar.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel10', 'js/calendar/demo.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel11', 'js/sortable/jquery.sortable.js');



        $theme->asset()->container('post-scripts')->usePath()->add('laravel6', 'js/pages/charts/flot.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel7', 'js/pages/charts/sparkline.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel9', 'js/pages/charts/jquery-knob.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel10', 'js/pages/index2.js');

        $theme->asset()->container('post-scripts')->add('laravel11', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular.min.js');
        $theme->asset()->container('post-scripts')->add('laravel12', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel13', 'js/pages/charts/angular-chart.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel14', 'js/app.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel15', 'js/modules/server.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel16', 'js/modules/zendesk.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel17', 'js/modules/signup.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel18', 'js/modules/txn.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel19', 'js/modules/renewal.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel20', 'js/modules/affiliate.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel21', 'js/modules/terminate.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel30', 'js/modules/merchant.angular.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel32', 'js/modules/txnalltime.angular.js?v=10');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel22', 'js/services/chart_server.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel23', 'js/services/chart_zendesk.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel24', 'js/services/chart_signup.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel25', 'js/services/chart_renewal.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel26', 'js/services/chart_affiliate.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel27', 'js/services/chart_txn.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel28', 'js/services/chart_terminate.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel29', 'js/services/chart_merchant.service.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel31', 'js/services/chart_txnalltime.service.js');


        $params = array();
        return $theme->scope('admin.angular', $params)->render();
    }

}
